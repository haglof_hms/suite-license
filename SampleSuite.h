#pragma once

#ifndef _SampleSuite_H_
#define _SampleSuite_H_

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>
#include "CLicProtectorEasyGo270.h"

typedef struct _tagLICENSE
{
	CString csModID;		// module id
	CString csModName;		// module name
	int nType;			// 1 = User, 2 = Computer, 3 = Counter, 4 = Yes/No, 5 = Item, 6 = Concurrent User (only possible if the necessary licence exists)
	int nLicenses;		// Number of users or computers etc. In a Yes/No-Module 1 for yes and 0 for no.
	BOOL bDemo;			// demo?
	DATE dtExpires;		// Date when this module will expire, 0 for unlimited
	int nNoOfDays;		// Number of days this module will run after the first usage
	DATE dtMaxDate;		// Max date when this module will no longer work, even if there are open days, 0 for unlimited
	CString csTag;		// The tag value of that module
	BOOL bAllowDeactivate;	// Can users, computers or items be deactivated In that module? 
	int nWebActivation;	// WebActivation State of the new module (0-3). See Web Activation Documentation. 

	BOOL bYesNo;		// TRUE = yes, FALSE = no
	int nRemDays;		// amount of days left
	int nRemLic;		// amount of licenses left
} LICENSE;

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

// Initialize the DLL, register the classes etc
extern "C" void DLL_BUILD InitSuite(CStringArray *,vecINDEX_TABLE &, vecINFO_TABLE &);

// Open a document view
extern "C" void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);
extern "C" void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret);

// license stuff
extern "C" int DLL_BUILD GetLicense(TCHAR *szName, LICENSE *lic, TCHAR *szSuite, BOOL bShow);	// used when wm is not prefered

extern "C" void DLL_BUILD CheckLicense(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);	// wm version
extern "C" void DLL_BUILD DecrCounter(_user_msg *msg,CWnd *,vecINDEX_TABLE &,int *ret);		// wm version


//class CLicCheckThread;
class CSampleSuite //: public CWinApp
{
	BOOL m_bActivated;
	BOOL m_bTampered;
	BOOL m_bDateChanged;
	BOOL m_bLicRefreshed;

public:
	CSampleSuite();
	~CSampleSuite();

	vecINFO_TABLE *m_vecInfoTable;

	CArray<LICENSE, LICENSE> m_caLicenses;

	int OpenLicense();
	int GatherLicenses(BOOL bWait = TRUE);
	int UpdateLicensefile();
	int GetLicense(CString csLicense);
	int GatherLicense(CString csModule);
	void WriteToLog(CStringA message, bool error=false);
	void GatherHwids();
	int GetCopyProtection();

	CString m_csComputername;
	CString m_csUsername;

	CString m_csLicfilePath;	// directory where licensefile reside
	BOOL m_bLicfile;	// do we have a licensefile?

	// used for contact information
	CString m_csCustNum;
	CString m_csName;
	CString m_csCompany;
	CString m_csVat;
	CString m_csAddress1;
	CString m_csAddress2;
	CString m_csAddress3;
	CString m_csCountry;
	CString m_csTelephone;
	CString m_csEmail;
	CString m_csNotes;

	BOOL m_bInUse;
	CString m_csInCustNum;
	CString m_csInName;
	CString m_csInCompany;
	CString m_csInVat;
	CString m_csInAddress1;
	CString m_csInAddress2;
	CString m_csInAddress3;
	CString m_csInCountry;
	CString m_csInTelephone;
	CString m_csInEmail;

	CString m_csEmailTo;
	CString m_csEmailSub;

	int CheckLicense(CString csLicense, _user_msg*);	// wm version
	int DecrCounter(CString csLicense, _user_msg*);		// wm version

	BOOL getActivated() { return m_bActivated; }
	BOOL getTampered() { return m_bTampered; }
	BOOL getDateChanged() { return m_bDateChanged; }

	BOOL IsRefreshed() { return m_bLicRefreshed; }
	void Refreshed(BOOL bRefresh) { m_bLicRefreshed = bRefresh; }
	int CheckTampered();

//	CString m_csInstCode;
	CStringArray m_caInstCodes;
	CString m_csActKey;

	CString m_csTempPath;

private:
	HANDLE m_hCheckThread;
};
extern CSampleSuite theApp;

extern RLFReader* g_pXML;
extern CLicProtectorEasyGo270 proxyLicProtector;	// Licence Protector class
extern /*const*/ CString sLicFile;
extern /*const*/ CString sPSK;
extern CString g_csLangFN;


DWORD WINAPI CheckRefresh(LPVOID lpParam);



template<class BASE_CLASS>
class CNoFlickerWnd : public BASE_CLASS
{

protected:
	virtual LRESULT WindowProc(UINT message,WPARAM wParam,LPARAM lParam)
	{
		switch (message)
		{
		case WM_PAINT:
			{
				CPaintDC dc(this);

				// Get the client rect, and paint to a memory device context.
				// This will help reduce screen flicker.  Pass the memory
				// device context to the default window procedure to do
				// default painting.

				CXTPClientRect rc(this);
				CXTPBufferDC memDC(dc, rc);
				memDC.FillSolidRect(rc, GetXtremeColor(COLOR_WINDOW));

				return BASE_CLASS::DefWindowProc(WM_PAINT,
					(WPARAM)memDC.m_hDC, 0);
			}

		case WM_ERASEBKGND:
				return TRUE;
		}

		return BASE_CLASS::WindowProc(message, wParam, lParam);
	}
};

#endif
