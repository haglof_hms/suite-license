#pragma once



// CInvoiceDlg form view

class CInvoiceDlg : public CFormView
{
	DECLARE_DYNCREATE(CInvoiceDlg)

protected:
	CInvoiceDlg();           // protected constructor used by dynamic creation
	virtual ~CInvoiceDlg();
public:
	CString m_csCustNum;
	CString m_csName;
	CString m_csCompany;
	CString m_csVat;
	CString m_csAddress1;
	CString m_csAddress2;
	CString m_csAddress3;
	CString m_csCountry;
	CString m_csTelephone;
	CString m_csEmail;


public:
	enum { IDD = IDD_INVOICE };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual void OnInitialUpdate();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckInvoice();
	BOOL m_bInvoice;
	afx_msg void OnClose();
	afx_msg void OnDestroy();
};


