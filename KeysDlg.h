#pragma once


// CKeysDlg dialog

class CKeysDlg : public CDialog
{
	DECLARE_DYNAMIC(CKeysDlg)

	CXTListCtrl m_listCtrl;

public:
	CKeysDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CKeysDlg();

// Dialog Data
	enum { IDD = IDD_KEYS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CString m_csActKey;
	afx_msg void OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult);
};
