// KeysDlg.cpp : implementation file
//

#include "stdafx.h"
#include "KeysDlg.h"
#include "Samplesuite.h"

// CKeysDlg dialog

IMPLEMENT_DYNAMIC(CKeysDlg, CDialog)

CKeysDlg::CKeysDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CKeysDlg::IDD, pParent)
	, m_csActKey(_T(""))
{
}

CKeysDlg::~CKeysDlg()
{
}

void CKeysDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_listCtrl);
	DDX_Text(pDX, IDC_EDIT_ACTKEY, m_csActKey);
}


BEGIN_MESSAGE_MAP(CKeysDlg, CDialog)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST1, &CKeysDlg::OnLvnItemchangedList1)
	ON_BN_CLICKED(IDC_BUTTON1, &CKeysDlg::OnBnClickedButton1)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CKeysDlg::OnNMClickList1)
END_MESSAGE_MAP()


// CKeysDlg message handlers

BOOL CKeysDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


	// disable the activation key if we already have one
	if(m_csActKey != "" || theApp.getActivated())
	{
		((CEdit*)GetDlgItem(IDC_EDIT_ACTKEY))->SetReadOnly(TRUE);
		GetDlgItem(IDC_EDIT_ACTKEY)->EnableWindow(FALSE);
		m_listCtrl.SetFocus();
	}
	
	SetWindowText(g_pXML->str(34));
	SetDlgItemText(IDC_STATIC_ACTKEY, g_pXML->str(35));
	SetDlgItemText(IDC_BUTTON1, g_pXML->str(36));

	m_listCtrl.ModifyExtendedStyle( 0, LVS_EX_FULLROWSELECT );
	m_listCtrl.ModifyExtendedStyle( 0, LVS_EX_GRIDLINES );


	// add columns
	m_listCtrl.InsertColumn(0, g_pXML->str(37), LVCFMT_LEFT, 220);	// _T("Registration keys")
	m_listCtrl.InsertColumn(1, g_pXML->str(38), LVCFMT_LEFT, 125);	// _T("Status")

	// add a empty row
	m_listCtrl.InsertItem(0, _T(""), 0);
	m_listCtrl.SetItem(0, 1, LVIF_TEXT, _T(""), 0, NULL, NULL, NULL);

	return TRUE;
}
void CKeysDlg::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVDISPINFOA pNMLV = reinterpret_cast<LPNMLVDISPINFOA>(pNMHDR);

	if(pNMLV->item.pszText == 0)
	{
		*pResult = FALSE;
		return;
	}

	if(strcmp(pNMLV->item.pszText, "") != 0)	// add another row
	{
		m_listCtrl.InsertItem(pNMLV->item.iItem, _T(""));
		*pResult = TRUE;
	}
	else	// remove this items row
	{
		if(m_listCtrl.GetItemCount() > 1)
			m_listCtrl.DeleteItem(pNMLV->item.iItem);

		*pResult = FALSE;
	}

	if(m_listCtrl.GetItemCount() > 1 || m_csActKey != "")
		GetDlgItem(IDC_BUTTON1)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_BUTTON1)->EnableWindow(FALSE);
}

// try to apply the keys
void CKeysDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);

	long lRes;
	CString csBuf;
	BOOL bError = FALSE;

	if( ((CEdit*)GetDlgItem(IDC_EDIT_ACTKEY))->GetModify() )
	{
		// apply activation key
		lRes = proxyLicProtector.ApplyActivationKey(m_csActKey);
		if(lRes == 0)	// ok
		{
		}
		else if(lRes == 258)	// wrong key
		{
			bError = TRUE;
		}
		else if(lRes == 257)	// already used
		{
			bError = TRUE;
		}
		else
		{
			AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes));
		}
	}

	// apply the registration keys
	for(int nLoop=m_listCtrl.GetItemCount()-1; nLoop>-1; nLoop--)
	{
		csBuf = m_listCtrl.GetItemText(nLoop, 0);

		if(csBuf != "")
		{
			lRes = proxyLicProtector.ApplyActivationKey(csBuf);
			if(lRes == 0)	// ok
			{
				// remove item
				m_listCtrl.DeleteItem(nLoop);
			}
			else if(lRes == 258)	// wrong key
			{
				bError = TRUE;
				m_listCtrl.SetItemText(nLoop, 1, g_pXML->str(39));	//"Wrong key!");
			}
			else if(lRes == 257)	// already used
			{
				bError = TRUE;
				m_listCtrl.SetItemText(nLoop, 1, g_pXML->str(40));	//"Already used!");
			}
			else
			{
				AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes));
			}
		}
	}

	if( !theApp.CheckTampered() ) return;
	proxyLicProtector.Refresh();
	theApp.GatherLicenses();

	if(bError == FALSE)
	{
		CDialog::OnOK();
	}
}

void CKeysDlg::OnNMClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	if(phdr->iItem != -1)	// user clicked a valid line
	{
		// set the row editable
		m_listCtrl.EditLabel(phdr->iItem);
	}

	*pResult = 0;
}
