// ProgramsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "LicenseView.h"
#include "ContactFormView.h"
#include "KeysDlg.h"
#include "InvoiceDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CLicenseDoc

IMPLEMENT_DYNCREATE(CLicenseDoc, CDocument)

BEGIN_MESSAGE_MAP(CLicenseDoc, CDocument)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLicenseDoc construction/destruction

CLicenseDoc::CLicenseDoc()
{
}

CLicenseDoc::~CLicenseDoc()
{
}

/////////////////////////////////////////////////////////////////////////////
// CLicenseDoc serialization

void CLicenseDoc::Serialize(CArchive& ar)
{
	CString csFilename, csBuf;
	csFilename = ar.GetFile()->GetFilePath();

	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

void CLicenseDoc::OnCloseDocument()
{
	CDocument::OnCloseDocument();
}


/////////////////////////////////////////////////////////////////////////////
// CLicenseDoc diagnostics

#ifdef _DEBUG
void CLicenseDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLicenseDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLicenseDoc commands




IMPLEMENT_DYNCREATE(CLicenseFrame, CMDIChildWnd)
BEGIN_MESSAGE_MAP(CLicenseFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CLicenseFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(ID_BUTTON_ORDER, OnBnClickedOrder)
	ON_BN_CLICKED(ID_BUTTON_KEYS, OnBnClickedLicense)
	ON_BN_CLICKED(ID_BUTTON_PATH, OnBnClickedPath)
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_WM_PAINT()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
};

/////////////////////////////////////////////////////////////////////////////
// CLicenseFrame construction/destruction

CLicenseFrame::CLicenseFrame()
{
	m_bKeys = FALSE;	// key-window not open
	m_bOnce = TRUE;
	m_pViewOne = NULL;
}

CLicenseFrame::~CLicenseFrame()
{
}

LRESULT CLicenseFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;
	};

	return 0;
}

void CLicenseFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_License\\Dialogs\\License"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CLicenseFrame::OnClose()
{
	if(m_bKeys == TRUE)
		return;

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CLicenseFrame::OnSetFocus(CWnd* pOldWnd)
{
	CMDIChildWnd::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
}

void CLicenseFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_License\\Dialogs\\License"), REG_ROOT);
	SavePlacement(this, csBuf);

	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

BOOL CLicenseFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLicenseFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient, WM_MDISETMENU, 0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CLicenseFrame diagnostics

#ifdef _DEBUG
void CLicenseFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLicenseFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLicenseFrame message handlers

int CLicenseFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CChildFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_PROGRAMS);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR_PROGRAMS)
				{		
						// order report
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(1));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 27);	// mail
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_ORDER);

						// enter keys
						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(2));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 52);	// licens
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_KEYS);

						// change path to licensefile
						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(60));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 47);	// verktyg
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);
						pCtrl->SetID(ID_BUTTON_PATH);
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))


	m_paneManager.InstallDockingPanes(this, TRUE);
	m_paneManager.SetTheme(xtpPaneThemeOffice2003);

	// Create docking panes.
	CXTPDockingPane* pwndPane1 = m_paneManager.CreatePane(1, CRect(0, 0, 330, 0), xtpPaneDockLeft);
	pwndPane1->SetTitle(g_pXML->str(24));	//_T("Invoice information"));
	pwndPane1->SetOptions(xtpPaneNoCloseable | xtpPaneNoFloatable);
	pwndPane1->Hide();

	UpdateWindow();

	return 0;
}

void CLicenseFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}

void CLicenseFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

void CLicenseFrame::OnBnClickedPath()
{
	if(!m_pViewOne)
	{
		CXTPDockingPane *pPane = m_paneManager.GetPaneList().GetHead().pPane;
		m_pViewOne = DYNAMIC_DOWNCAST(CInvoiceDlg, pPane->AttachView(this, RUNTIME_CLASS(CInvoiceDlg)));
	}

	((CContactFormView*)GetActiveView())->OnPath();
}

void CLicenseFrame::OnBnClickedLicense()
{
	// show the key-dialog
	m_bKeys = TRUE;

	CKeysDlg dlg;
	dlg.m_csActKey = theApp.m_csActKey;
	dlg.DoModal();

	m_bKeys = FALSE;
}

void CLicenseFrame::OnBnClickedOrder()
{
	if(!m_pViewOne)
	{
		CXTPDockingPane *pPane = m_paneManager.GetPaneList().GetHead().pPane;
		m_pViewOne = DYNAMIC_DOWNCAST(CInvoiceDlg, pPane->AttachView(this, RUNTIME_CLASS(CInvoiceDlg)));
	}

	((CContactFormView*)GetActiveView())->OnOrder();
}

void CLicenseFrame::OnBnClickedSendLicense()
{
	((CContactFormView*)GetActiveView())->OnSendLicense();
}

void CLicenseFrame::OnBnClickedGetLicense()
{
	((CContactFormView*)GetActiveView())->OnGetLicense();
}


LRESULT CLicenseFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		CXTPDockingPane* pPane = (CXTPDockingPane*)lParam;

		if (!pPane->IsValid())
		{
			switch (pPane->GetID())
			{
			case 1:
				{
					if (!m_pViewOne)
					{
						m_pViewOne = DYNAMIC_DOWNCAST(CInvoiceDlg, pPane->AttachView(this, RUNTIME_CLASS(CInvoiceDlg)));
						m_pViewOne->m_csAddress1 = _T("");
						m_pViewOne->m_csAddress2 = _T("");
						m_pViewOne->m_csAddress3 = _T("");
						m_pViewOne->m_csCompany = _T("");
						m_pViewOne->m_csCountry = _T("");
						m_pViewOne->m_csCustNum = _T("");
						m_pViewOne->m_csEmail = _T("");
						m_pViewOne->m_csName = _T("");
						m_pViewOne->m_csTelephone = _T("");
						m_pViewOne->m_csVat = _T("");
						m_pViewOne->ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
					}
					else
					{
						pPane->Attach(m_pViewOne->GetParent());
					}
					break;
				}
			}
		}
		return TRUE;
	}
	return FALSE;
}

void CLicenseFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}

	CMDIChildWnd::OnPaint();
}
