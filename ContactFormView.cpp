// ContactFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "ContactFormView.h"
#include "samplesuite.h"
#include "LicenseView.h"
#include "MessageRecord.h"
#include "CustomItems.h"
#include "LicenseDlg.h"

// CContactFormView

IMPLEMENT_DYNCREATE(CContactFormView, CXTResizeFormView)

CContactFormView::CContactFormView()
	: CXTResizeFormView(CContactFormView::IDD)
, m_csCustNum(_T(""))
, m_csName(_T(""))
, m_csCompany(_T(""))
, m_csVat(_T(""))
, m_csAddress1(_T(""))
, m_csAddress2(_T(""))
, m_csAddress3(_T(""))
, m_csCountry(_T(""))
, m_csTelephone(_T(""))
, m_csEmail(_T(""))
, m_csNotes(_T(""))
{
	m_bOnce = FALSE;
	m_bFirst = TRUE;
	m_bResize = TRUE;
}

CContactFormView::~CContactFormView()
{
}

void CContactFormView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CUSTNUM, m_csCustNum);
	DDX_Text(pDX, IDC_EDIT_NAME, m_csName);
	DDX_Text(pDX, IDC_EDIT_COMPANY, m_csCompany);
	DDX_Text(pDX, IDC_EDIT_VAT, m_csVat);
	DDX_Text(pDX, IDC_EDIT_ADDRESS1, m_csAddress1);
	DDX_Text(pDX, IDC_EDIT_ADDRESS2, m_csAddress2);
	DDX_Text(pDX, IDC_EDIT_ADDRESS3, m_csAddress3);
	DDX_Text(pDX, IDC_EDIT_COUNTRY, m_csCountry);
	DDX_Text(pDX, IDC_EDIT_TELEPHONE, m_csTelephone);
	DDX_Text(pDX, IDC_EDIT_EMAIL, m_csEmail);
	DDX_Text(pDX, IDC_EDIT_NOTES, m_csNotes);
}

BEGIN_MESSAGE_MAP(CContactFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_LBN_DBLCLK(IDC_LIST_CODES, OnLbnSelchangeListCodes)
	ON_BN_CLICKED(IDC_BUTTON_COPYCLIP, OnBnClickedButtonCopyclip)
END_MESSAGE_MAP()


// CContactFormView diagnostics

#ifdef _DEBUG
void CContactFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CContactFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CContactFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	/*if(m_bResize == TRUE)
	{
		if(GetDlgItem(IDC_REPORT_LICENSES) != NULL)
			GetDlgItem(IDC_REPORT_LICENSES)->SetWindowPos(NULL, 0, 0, cx-1, cy-1, SWP_NOMOVE|SWP_NOOWNERZORDER|SWP_NOZORDER);
	}*/
}

BOOL CContactFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	//cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	//cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CContactFormView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	return 0;
}

// CContactFormView message handlers
void CContactFormView::OnSetFocus(CWnd* pOldWnd)
{
	if(m_bFirst == TRUE)
	{
		m_bFirst = FALSE;

		// do we have a licensefile loaded?
		if(theApp.m_bLicfile == FALSE)
		{
			OnPath();
		}
	}

	if(theApp.IsRefreshed())
	{
		AddRecords();
		theApp.Refreshed(FALSE);
	}

	CView::OnSetFocus(pOldWnd);
}

void CContactFormView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
//	ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	theApp.GatherHwids();
	theApp.GatherLicenses(FALSE);

	((CLicenseFrame*)GetParentFrame())->SetButtonEnabled(0, TRUE);	// send order
	((CLicenseFrame*)GetParentFrame())->SetButtonEnabled(1, TRUE);	// licenses
	((CLicenseFrame*)GetParentFrame())->SetButtonEnabled(2, TRUE);	// path

	m_csCustNum = theApp.m_csCustNum;
	m_csName = theApp.m_csName;
	m_csCompany = theApp.m_csCompany;
	m_csVat = theApp.m_csVat;
	m_csAddress1 = theApp.m_csAddress1;
	m_csAddress2 = theApp.m_csAddress2;
	m_csAddress3 = theApp.m_csAddress3;
	m_csCountry = theApp.m_csCountry;
	m_csTelephone = theApp.m_csTelephone;
	m_csEmail = theApp.m_csEmail;
	m_csNotes = theApp.m_csNotes;


	SetDlgItemText(IDC_STATIC_CUSTNUM, g_pXML->str(3));
	SetDlgItemText(IDC_STATIC_NAME, g_pXML->str(4));
	SetDlgItemText(IDC_STATIC_COMPANY, g_pXML->str(5));
	SetDlgItemText(IDC_STATIC_VAT, g_pXML->str(6));
	SetDlgItemText(IDC_STATIC_ADDRESS, g_pXML->str(7));
	SetDlgItemText(IDC_STATIC_COUNTRY, g_pXML->str(8));
	SetDlgItemText(IDC_STATIC_TELEPHONE, g_pXML->str(9));
	SetDlgItemText(IDC_STATIC_EMAIL, g_pXML->str(10));
	SetDlgItemText(IDC_STATIC_CODES, g_pXML->str(11));
	SetDlgItemText(IDC_STATIC_NOTES, g_pXML->str(12));
	SetDlgItemText(IDC_BUTTON_COPYCLIP, g_pXML->str(48));

	CString csBuf;
	if(theApp.GetCopyProtection() > 0)
	{
		csBuf.Format(_T("%s (%d: %s)"), g_pXML->str(13), theApp.GetCopyProtection(), theApp.m_caInstCodes.GetAt(theApp.GetCopyProtection()-1));
	}
	else
	{
		csBuf.Format(_T("%s"), g_pXML->str(13));
	}
	SetDlgItemText(IDC_STATIC_LICENSES, csBuf);


	// get the size of the placeholder, this will be used when creating the report.
	CRect rc;
	GetDlgItem(IDC_PLACEHOLDER)->GetWindowRect(&rc);
	ScreenToClient( &rc );

	// create the report
	if ( m_wndReportCtrl.Create( WS_BORDER | WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL, rc, this, IDC_REPORT_LICENSES ) )
	{
		// Add columns
		CXTPReportColumn* pCol;
		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(0, g_pXML->str(14), 50));	// "Description"
		m_pColDesc = pCol;
		pCol->AllowRemove(FALSE);
		m_wndReportCtrl.GetColumns()->SetSortColumn(pCol, TRUE);

		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(1, g_pXML->str(43), 20));	// "Version"
		m_pColVersion = pCol;
		pCol->AllowRemove(FALSE);

		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(2, g_pXML->str(15), 50));	// "Article"
		m_pColArticle = pCol;
		pCol->AllowRemove(FALSE);

		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(3, g_pXML->str(16), 50));	// "Key(s)"
		pCol->AllowRemove(FALSE);

		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(4, g_pXML->str(17), 50));	// "Status"
		pCol->AllowRemove(FALSE);

		pCol = m_wndReportCtrl.AddColumn(new CXTPReportColumn(5, g_pXML->str(18), 50));	// "Order?"
		m_pColOrder = pCol;
		pCol->AllowRemove(FALSE);

		m_wndReportCtrl.SelectionEnable(FALSE);
		SetResize(IDC_REPORT_LICENSES, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

		AddRecords();

		theApp.Refreshed(FALSE);
	}

	UpdateData(FALSE);

	SetScaleToFitSize(CSize(1, 1)); 
}

// Add data
void CContactFormView::AddRecords()
{
	CString csBuf, csBuf2, csName, csTmp, csUsers;
	int nLoop;
	LICENSE lic;
	CXTPReportRecord *pRec, *pChildRec;
	CString sLangFileName, sName, m_sLangAbrev = getLangSet();
	vecINFO_TABLE m_vecInfoTable = *theApp.m_vecInfoTable;
	BOOL bFound;
	CString csDesc, csVersion, csArticle, csKeys, csStatus;
	BOOL bOrder;
	RLFReader xml;
	CString csLangFN, csLang;
	INFO_TABLE rec = m_vecInfoTable[0];
	CStringArray csaUsers;
	int curPos = 0;


	// fill the codes list
	CListBox* clbCodes = (CListBox*)GetDlgItem(IDC_LIST_CODES);
	clbCodes->ResetContent();

	for(nLoop=0; nLoop<theApp.m_caInstCodes.GetSize(); nLoop++)
	{
		if(theApp.m_caInstCodes.GetAt(nLoop) != "")
		{
			csBuf.Format(_T("%d: %s"), nLoop+1, theApp.m_caInstCodes.GetAt(nLoop));
			clbCodes->AddString(csBuf);
		}
	}

	clbCodes->SetCurSel( theApp.GetCopyProtection() -1 );


	m_wndReportCtrl.ResetContent();

	// see if the Timber cruise module is loaded
	BOOL bTCruise = FALSE;
	for (UINT i = 0; i < m_vecInfoTable.size(); i++)
	{
		rec = m_vecInfoTable[i];
		sName = rec.szLanguageFN;
		sName = sName.Right(sName.GetLength() - sName.ReverseFind('\\') - 1);

		if(sName.MakeUpper() == _T("TIMBERCRUISE"))
		{
			bTCruise = TRUE;
			break;
		}
	}


	// put all the suites and modules into it.
	for(int nLoop=0; nLoop<theApp.m_caLicenses.GetSize(); nLoop++)
	{
		csStatus = "";
		csBuf = "";
		bFound = FALSE;
		lic = theApp.m_caLicenses.GetAt(nLoop);
		csUsers = "";
		csaUsers.RemoveAll();

		// get all current users using this license
		csUsers = proxyLicProtector.GetEntriesOfModule(lic.csModID, true, _T(";"));	// "UT0003+anders+++UT0003"
		curPos = 0;
		CString resToken = csUsers.Tokenize(_T(";"),curPos);
		while (resToken != _T(""))
		{
			csBuf = resToken.Left(resToken.Find(_T("+++")));
			csBuf.Replace('+', '\\');
			if(csBuf != _T("")) csaUsers.Add(csBuf);

			resToken = csUsers.Tokenize(_T("% #"), curPos);
		};


		if(lic.csModID == _T("H3000") && bTCruise == TRUE)	// TCruise
		{
			bFound = TRUE;
			csVersion = _T("");
			sName = _T("TCruise");

			// get path to TCruise.exe from the registry
			CString csPathTCruise = _T("");
			csPathTCruise = regGetStr(_T("Software\\Heuristic Solutions Apps"), _T("Heuristic Solutions TCruise"), _T("InstallDir"), _T(""));
			if(csPathTCruise != _T(""))
			{
				if(csPathTCruise.Right(1) != _T("\\")) csPathTCruise += _T("\\");
				csPathTCruise += _T("TCruise.exe");

				// get the version from TCruise.exe
				csVersion = getVersionInfo(csPathTCruise, VER_NUMBER);
				if(csVersion != _T("")) csVersion.Replace(_T(", "), _T("."));
			}

			if(lic.bDemo == TRUE)
			{
				csStatus = g_pXML->str(19);	//.Format(_T("Demo."));

				if(lic.nRemDays == 0)	// expired
				{
					csStatus += " " + g_pXML->str(20);
				}
				else 
				{
					if(lic.nRemDays > 0)	// days remaining
					{
						csBuf.Format(_T(" %d %s"), lic.nRemDays, g_pXML->str(21));
						csStatus += " " + csBuf;
					}

					if(lic.nRemLic > 0)	// item remaining
					{
						if(lic.nType != 6)
						{
							csBuf.Format(_T(" %d %s"), lic.nRemLic, g_pXML->str(22));
							csStatus += " " + csBuf;
						}
					}
				}
			}
			else
			{
				if(lic.nType == 6)	// concurrent user
				{
					if(lic.nRemLic == -1)	//if(lic.nRemLic == -1)
					{
						csStatus = g_pXML->str(47);	// locked
					}
					else
					{
						csStatus = g_pXML->str(23);	// active
						csBuf.Format(_T(" %d/%d %s"), lic.nLicenses-lic.nRemLic, lic.nLicenses, g_pXML->str(46));
						csStatus += " " + csBuf;
					}
				}
				else	// module active
				{
					csStatus = g_pXML->str(23);
				}
			}

			csKeys = proxyLicProtector.GetVal(lic.csModID, _T("KYS"));
			csKeys.Replace(_T(";"), _T(" "));

			csBuf = lic.csModID.Right(lic.csModID.GetLength()-1);
			csArticle = _T("14-101-") + csBuf;

			bOrder = lic.bDemo;
		}
		else	// all others except TCruise
		{
			for (UINT i = 0; i < m_vecInfoTable.size(); i++)
			{
				rec = m_vecInfoTable[i];
				sName = rec.szLanguageFN;
				sName = sName.Right(sName.GetLength() - sName.ReverseFind('\\') - 1);

				if(sName.MakeUpper() == lic.csTag.MakeUpper())
				{
					bFound = TRUE;

					// get name and description from the suite/modules language file
					csLangFN.Format(_T("%s"), rec.szLanguageFN);
					csLang = getLangSet();
					if(!xml.Load(csLangFN + csLang + _T(".xml")))
						xml.Load(csLangFN + _T("ENU.xml"));
					sName = xml.str(-999);
					xml.clean();

					if(lic.bDemo == TRUE)
					{
						csStatus = g_pXML->str(19);	//.Format(_T("Demo."));

						if(lic.nRemDays == 0)	// expired
						{
							csStatus += " " + g_pXML->str(20);
						}
						else 
						{
							if(lic.nRemDays > 0)	// days remaining
							{
								csBuf.Format(_T(" %d %s"), lic.nRemDays, g_pXML->str(21));
								csStatus += " " + csBuf;
							}

							if(lic.nRemLic > 0)	// item remaining
							{
								if(lic.nType != 6)
								{
									csBuf.Format(_T(" %d %s"), lic.nRemLic, g_pXML->str(22));
									csStatus += " " + csBuf;
								}
							}
						}
					}
					else
					{
						if(lic.nType == 6)	// concurrent user
						{
							if(lic.nRemLic == -1)	//if(lic.nRemLic == -1)
							{
								csStatus = g_pXML->str(47);	// locked
							}
							else
							{
								csStatus = g_pXML->str(23);	// active
								csBuf.Format(_T(" %d/%d %s"), lic.nLicenses-lic.nRemLic, lic.nLicenses, g_pXML->str(46));
								csStatus += " " + csBuf;
							}
						}
						else	// module active
						{
							csStatus = g_pXML->str(23);
						}
					}

					csKeys = proxyLicProtector.GetVal(lic.csModID, _T("KYS"));
					csKeys.Replace(_T(";"), _T(" "));

					csBuf = lic.csModID.Right(lic.csModID.GetLength()-1);
					csArticle = _T("14-101-") + csBuf;

					bOrder = lic.bDemo;

					csVersion = rec.szVersion;

					break;
				}

			}	// loop vectors
		}	// not TCruise

		if(bFound == TRUE)
		{
			pRec = new CMessageRecord(sName, csVersion, csArticle, csKeys, csStatus, bOrder);
			m_wndReportCtrl.AddRecordEx(pRec);

			if(csaUsers.GetSize() > 0)
			{
				for(int nLoop=0; nLoop<csaUsers.GetSize(); nLoop++)
				{
					pRec->GetChilds()->Add( new CMessageRecord(_T(""), _T(""), _T(""), _T(""), csaUsers.GetAt(nLoop), false) );
				}
			}
		}
	}

	m_wndReportCtrl.GetColumns()->Find(0)->SetTreeColumn(TRUE);
	m_wndReportCtrl.Populate();
	m_wndReportCtrl.CollapseAll();
}


void CContactFormView::OnOrder()
{
	CXTPReportRecords *pRecs, *pChilds;
	CXTPReportRecord* pRec;
	CXTPReportRecordItem* pItem;
	CString csDesc, csArticle, csOrder;
	BOOL bOrder;

	UpdateData(TRUE);
	pRecs = m_wndReportCtrl.GetRecords();

	// show the report/order
	CInvoiceDlg* pInvoiceDlg;
	pInvoiceDlg = ((CInvoiceDlg*)((CLicenseFrame*)GetParentFrame())->m_pViewOne);


	// make sure that all necessary variables are entered
	BOOL bMissing = FALSE;
	CString csBuf;
	csBuf.Format(_T("%s\r\n"), g_pXML->str(49));
	if(m_csName == _T(""))
	{
		csBuf += g_pXML->str(27);	//_T("name\r\n");
		csBuf += _T("\r\n");
		bMissing = TRUE;
	}
	if(m_csAddress1 == _T(""))
	{
		csBuf += g_pXML->str(30);	//_T("address\r\n");
		csBuf += _T("\r\n");
		bMissing = TRUE;
	}
	if(m_csCountry == _T(""))
	{
		csBuf += g_pXML->str(31);	//_T("country\r\n");
		csBuf += _T("\r\n");
		bMissing = TRUE;
	}
	if(m_csTelephone == _T(""))
	{
		csBuf += g_pXML->str(32);	//_T("telephone");
		csBuf += _T("\r\n");
		bMissing = TRUE;
	}

	if(bMissing == TRUE)
	{
		MessageBox(csBuf, g_pXML->str(-999), MB_ICONERROR);
		return;
	}

	CString csInst = _T("");
	for(int nLoop=0; nLoop<theApp.m_caInstCodes.GetSize(); nLoop++)
	{
		csBuf.Format(_T("%d:%s "), nLoop+1, theApp.m_caInstCodes.GetAt(nLoop));
		csInst.Append(csBuf);
	}


	if(!pInvoiceDlg) return;
	pInvoiceDlg->UpdateData(TRUE);

	if(pInvoiceDlg->m_bInvoice)
	{
		csOrder.Format(_T("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;email_to=%s;email_subject=%s;code_used=%d;"),	// 22 + 2
			m_csCustNum, m_csName, m_csCompany, m_csVat,
			m_csAddress1, m_csAddress2, m_csAddress3,
			m_csCountry, m_csTelephone, m_csEmail,
			pInvoiceDlg->m_csCustNum, pInvoiceDlg->m_csName, pInvoiceDlg->m_csCompany,
			pInvoiceDlg->m_csVat, pInvoiceDlg->m_csAddress1, pInvoiceDlg->m_csAddress2,
			pInvoiceDlg->m_csAddress3, pInvoiceDlg->m_csCountry, pInvoiceDlg->m_csTelephone,
			pInvoiceDlg->m_csEmail,
			csInst,
			m_csNotes,
			theApp.m_csEmailTo, theApp.m_csEmailSub, theApp.GetCopyProtection());
	}
	else
	{
		csOrder.Format(_T("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";\"\";%s;%s;email_to=%s;email_subject=%s;code_used=%d;"),	// 22 + 2
			m_csCustNum, m_csName, m_csCompany, m_csVat,
			m_csAddress1, m_csAddress2, m_csAddress3,
			m_csCountry, m_csTelephone, m_csEmail,
			csInst,
			m_csNotes,
			theApp.m_csEmailTo, theApp.m_csEmailSub, theApp.GetCopyProtection());
	}

	for(int nLoop=0; nLoop<pRecs->GetCount(); nLoop++)
	{
		pRec = pRecs->GetAt(nLoop);

		pItem = pRec->GetItem(m_pColOrder->GetItemIndex());	// order?
		bOrder = pItem->IsChecked();

		if(bOrder)
		{
			pItem = pRec->GetItem(m_pColDesc->GetItemIndex());	// description
			csDesc = pItem->GetCaption(0);

			pItem = pRec->GetItem(m_pColArticle->GetItemIndex());	// article
			csArticle = pItem->GetCaption(0);

			csOrder = csOrder + csArticle + "    " + csDesc + "@";
		}

		if(pRec->HasChildren())
		{
			pChilds = pRec->GetChilds();

			for(int nLoop2=0; nLoop2<pChilds->GetCount(); nLoop2++)
			{
				pRec = pChilds->GetAt(nLoop2);

				pItem = pRec->GetItem(m_pColOrder->GetItemIndex());	// order?
				bOrder = pItem->IsChecked();

				if(bOrder)
				{
					pItem = pRec->GetItem(m_pColDesc->GetItemIndex());	// description
					csDesc = pItem->GetCaption(0);

					pItem = pRec->GetItem(m_pColArticle->GetItemIndex());	// article
					csArticle = pItem->GetCaption(0);

					csOrder = csOrder + csArticle + "    " + csDesc + "@";
				}
			}
		}
	}

	if(csArticle == "")
	{
		MessageBox(g_pXML->str(51), g_pXML->str(-999), MB_ICONERROR);
		return;	// nothing checked
	}

	// show the license report
	CString csReport;
	CString csLang = getLangSet();

	csReport.Format(_T("%s%s\\HMS_licenseorder%s.rpt"), getReportsDir(), csLang, csLang);
	if(!fileExists(csReport))
	{
		csReport.Format(_T("%sENU\\HMS_licenseorderENU.rpt"), getReportsDir());
	}

	// tell the report-suite to open the report
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(333, _T("OpenSuiteEx"),	
		_T("Reports2.dll"),
		csReport,
		_T(""),
		_T(""),
		(void*)&csOrder));
}
void CContactFormView::OnDestroy()
{
	// save dialog values
	UpdateData(TRUE);

	theApp.m_csCustNum = m_csCustNum;
	theApp.m_csName = m_csName;
	theApp.m_csCompany = m_csCompany;
	theApp.m_csVat = m_csVat;
	theApp.m_csAddress1 = m_csAddress1;
	theApp.m_csAddress2 = m_csAddress2;
	theApp.m_csAddress3 = m_csAddress3;
	theApp.m_csCountry = m_csCountry;
	theApp.m_csTelephone = m_csTelephone;
	theApp.m_csEmail = m_csEmail;
	theApp.m_csNotes = m_csNotes;

	CXTResizeFormView::OnDestroy();
}

// send a license to the web
void CContactFormView::OnSendLicense()
{
	long lRes = proxyLicProtector.WebRegister(_T(""), _T("prod"), _T("key"), _T("email"));
	if(lRes == 0)	// success
	{
	}
	else if(lRes == 3001)	// server not reachable
	{
	}
	else
	{
	}

	proxyLicProtector.CheckLicence(_T(""), _T("key"), _T("prod"), TRUE);
	proxyLicProtector.DeactivateLicence(_T(""), _T("key"));
}

// get a license from the web
void CContactFormView::OnGetLicense()
{
	proxyLicProtector.CheckLicence(_T(""), _T("key"), _T("prod"), TRUE);
}

void CContactFormView::OnPath()
{
	CLicenseDlg dlg;
	dlg.m_csPath = theApp.m_csLicfilePath;
	int nRet = dlg.DoModal();

	if(nRet == IDOK)
	{
		theApp.m_csLicfilePath = dlg.m_csPath;
		if(theApp.m_csLicfilePath.GetAt(theApp.m_csLicfilePath.GetLength()-1) != '\\')
			theApp.m_csLicfilePath += _T("\\");

		regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), theApp.m_csLicfilePath);

		// reload the license file
		theApp.OpenLicense();
		theApp.GatherHwids();
		theApp.GatherLicenses();

		AddRecords();
		theApp.Refreshed(FALSE);
	}
}
void CContactFormView::OnLbnSelchangeListCodes()
{
	// copy selected code to clipboard
	CListBox *pBox = (CListBox*)GetDlgItem(IDC_LIST_CODES);
	if(pBox->GetCurSel() == LB_ERR) return;

	CString csBuf;
	pBox->GetText(pBox->GetCurSel(), csBuf);

	OpenClipboard();
	EmptyClipboard();

	// Allocate a global memory object for the text. 
	HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, (csBuf.GetLength() + 1) * sizeof(TCHAR)); 
	if (hglbCopy == NULL) 
	{ 
		CloseClipboard(); 
		return;
	} 

	// Lock the handle and copy the text to the buffer. 
	LPTSTR lptstrCopy = (LPTSTR)GlobalLock(hglbCopy); 
	memcpy(lptstrCopy, csBuf.GetBuffer(), csBuf.GetLength() * sizeof(TCHAR)); 
	lptstrCopy[csBuf.GetLength()] = (TCHAR)0;    // null character 
	GlobalUnlock(hglbCopy); 

	SetClipboardData(CF_UNICODETEXT, hglbCopy);
	CloseClipboard();

	MessageBox(g_pXML->str(61), g_pXML->str(820), MB_ICONINFORMATION);
}

void CContactFormView::OnBnClickedButtonCopyclip()
{
	OnLbnSelchangeListCodes();
}
