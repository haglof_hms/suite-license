// ProgramDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Samplesuite.h"
#include "ProgramDlg.h"
#include "ProgramsDlg.h"
#include "MessageRecord.h"
#include "Usefull.h"
#include ".\programdlg.h"

IMPLEMENT_DYNCREATE(CProgramFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CProgramFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CProgramFrame)
	ON_WM_CREATE()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame construction/destruction

CProgramFrame::CProgramFrame()
{
	m_bOnce = TRUE;
}

CProgramFrame::~CProgramFrame()
{
}

LRESULT CProgramFrame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
			((CProgramDlg*)GetActiveView())->OnSave();
		break;

		case ID_DELETE_ITEM:
			((CProgramDlg*)GetActiveView())->OnDelete();
		break;
	};

	return 0;
}

void CProgramFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce)
    {
		m_bOnce = false;

		CString csBuf;
		csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Program"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

void CProgramFrame::OnClose()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CProgramFrame::OnDestroy()
{
	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\HMS_Development\\Dialogs\\Program"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;
}

BOOL CProgramFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CChildFrameBase::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CProgramFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CChildFrameBase::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
        RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame diagnostics

#ifdef _DEBUG
void CProgramFrame::AssertValid() const
{
	CChildFrameBase::AssertValid();
}

void CProgramFrame::Dump(CDumpContext& dc) const
{
	CChildFrameBase::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProgramFrame message handlers

int CProgramFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if(CChildFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;
/*
	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this, 0);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR_PROGRAM);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgramDir() + _T("HMSToolBarIcons32.dll");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR_PROGRAM)
				{		
					hResModule = LoadLibrary(_T(sTBResFN));
					if (hResModule)
					{
						// load/send program
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(1025));
						hIcon = LoadIcon(hResModule, _T("HMI"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(1026));
						hIcon = LoadIcon(hResModule, _T("ADD"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(1027));
						hIcon = LoadIcon(hResModule, _T("MINUS"));
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
						pCtrl->SetFlags(xtpFlagManualUpdate);
						pCtrl->SetEnabled(FALSE);

						FreeLibrary(hResModule);
					}	// if (hResModule)

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
*/
	UpdateWindow();

	return 0;
}

void CProgramFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
/*
	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}*/
}

void CProgramFrame::SetButtonEnabled(UINT nID, BOOL bEnabled)
{
	CXTPToolBar* pToolBar = &m_wndToolBar;
	CXTPControls* p = pToolBar->GetControls();
	CXTPControl* pCtrl = NULL;

	pCtrl = p->GetAt(nID);
	pCtrl->SetEnabled(bEnabled);
}

// CProgramDlg

#define IDC_REPORT 9998
#define COLUMN_SERIAL 0
#define COLUMN_KEY 1


IMPLEMENT_DYNCREATE(CProgramDlg, CXTResizeFormView)

CProgramDlg::CProgramDlg()
	: CXTResizeFormView(CProgramDlg::IDD)
	, m_csName(_T(""))
	, m_csID(_T(""))
	, m_nType(0)
	, m_csVersion(_T(""))
	, m_nLanguage(0)
	, m_csDescription(_T(""))
	, m_bNeedLicense(FALSE)
{
	m_csLang = _T("");
	m_bNew = FALSE;
	m_nSelProgram = -1;
	m_nSelLang = -1;
	m_bDontBother = FALSE;
}

CProgramDlg::~CProgramDlg()
{
}

void CProgramDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgramDlg)
	DDX_Text(pDX, IDC_EDIT_NAME, m_csName);
	DDX_Text(pDX, IDC_EDIT_ID, m_csID);
	DDX_CBIndex(pDX, IDC_COMBO_TYPE, m_nType);
	DDX_Text(pDX, IDC_EDIT_VERSION, m_csVersion);
	DDX_CBIndex(pDX, IDC_COMBO_LANGUAGE, m_nLanguage);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_REPORT, m_wndReport);
	DDX_Text(pDX, IDC_EDIT_DESCRIPTION, m_csDescription);
	DDX_Check(pDX, IDC_CHECK_NEEDLICENSE, m_bNeedLicense);
}

BEGIN_MESSAGE_MAP(CProgramDlg, CXTResizeFormView)
	//{{AFX_MSG_MAP(CProgramDlg)
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DROPFILES()
	ON_NOTIFY(XTP_NM_REPORT_INPLACEBUTTONDOWN, IDC_REPORT, OnReportFilebutton)
	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, IDC_REPORT, OnReportSelChanged)
	ON_NOTIFY(NM_CLICK, IDC_REPORT, OnReportSelChanged)
END_MESSAGE_MAP()


// CProgramDlg diagnostics

#ifdef _DEBUG
void CProgramDlg::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CProgramDlg::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CProgramDlg message handlers
BOOL CProgramDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CProgramDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create the reportcontrol
	if (!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL, CRect(10, 300, lpCreateStruct->cx - 10, lpCreateStruct->cy - 10), this, IDC_REPORT))
	{
		TRACE(_T("Failed to create reportcontrol"));
		return -1;
	}

	m_wndReport.SetGridStyle(FALSE, xtpReportGridNoLines);
	m_wndReport.GetPaintManager()->m_columnStyle = xtpReportColumnExplorer;

	return 0;
}

void CProgramDlg::OnInitialUpdate()
{
	// get a handle to the document
	pDoc = (CMDISampleSuiteDoc*)GetDocument();


	// setup the reportcontrol
	m_wndReport.ModifyStyle(0, WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP);
	CXTPReportColumn* pCol = m_wndReport.AddColumn(new CXTPReportColumn(0, _T("Filename"), 50, TRUE, XTP_REPORT_NOICON, FALSE));
	pCol->AllowRemove(FALSE);
	pCol = m_wndReport.AddColumn(new CXTPReportColumn(1, _T("Type"), 50, TRUE, XTP_REPORT_NOICON, FALSE));
	pCol->AllowRemove(FALSE);


	m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
	m_wndReport.DragAcceptFiles();


	if(pDoc->m_bNew != FALSE)
	{
		m_bNew = TRUE;
	}

	CXTResizeFormView::OnInitialUpdate();


	// limit the description to 512 bytes
	((CEdit*)GetDlgItem(IDC_EDIT_DESCRIPTION))->SetLimitText(512);


	// fill the comboboxes
	// type
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	pCombo->AddString(_T("DigitechPro"));
/*	pCombo->AddString(_T("Mantax computer"));
	pCombo->AddString(_T("PocketPC"));
	pCombo->AddString(_T("WinCE"));
	pCombo->AddString(_T("Windows"));*/

	// language
	int nLoop=0, nFound = 0;
	CString csBuf;

	pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE);
	do
	{
		csBuf = m_cLocale.GetLangString(nLoop);
		pCombo->AddString(csBuf);

		if(csBuf == m_csLang) nFound = nLoop;

		nLoop++;
	}
	while(csBuf != _T(""));
	m_nLanguage = nFound;


	// Set the resize
//	SetResize(IDC_EDIT_DESCRIPTION, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_STATIC_FILES, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	SetResize(IDC_REPORT, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
	

	// resize the window to match the frame
	RECT rect;
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

//	ResizeParentToFit();

	UpdateData(FALSE);
}

void CProgramDlg::OnSetFocus(CWnd* pOldWnd)
{
	CString csBuf;

	CView::OnSetFocus(pOldWnd);

	if(m_bDontBother == TRUE) return;


	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	if(pDoc->m_bLock == FALSE)
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
	else
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	// save if there is any changes?


	// fill the dialog
	if(pDoc->m_bNew == TRUE)
	{
		GetDlgItem(IDC_EDIT_NAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_ID)->EnableWindow(TRUE);
		GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(TRUE);

		if(pDoc->m_bClick == TRUE)
		{
			m_csName = _T("");
			m_csID = _T("");
			m_nType = 0;
			m_csVersion = _T("");
			m_csDescription = _T("");
			m_csLang = _T("");
			m_nType = theApp.m_nType;

			m_nSelProgram = -1;
			m_nSelLang = -1;

			// remove files in the reportcontrol
			m_caFiles.RemoveAll();
			m_wndReport.GetRecords()->RemoveAll();
			m_wndReport.Populate();

			UpdateData(FALSE);

			pDoc->m_bClick = FALSE;
		}
	}
	else
	{
		GetDlgItem(IDC_EDIT_NAME)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_ID)->EnableWindow(FALSE);
		GetDlgItem(IDC_COMBO_TYPE)->EnableWindow(FALSE);

		if(pDoc->m_bLock == TRUE)
		{
			GetDlgItem(IDC_EDIT_VERSION)->EnableWindow(FALSE);
			GetDlgItem(IDC_COMBO_LANGUAGE)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_DESCRIPTION)->EnableWindow(FALSE);
			GetDlgItem(IDC_CHECK_NEEDLICENSE)->EnableWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_EDIT_VERSION)->EnableWindow(TRUE);
			GetDlgItem(IDC_COMBO_LANGUAGE)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_DESCRIPTION)->EnableWindow(TRUE);
			GetDlgItem(IDC_CHECK_NEEDLICENSE)->EnableWindow(TRUE);
		}

		int nProg, nLang;
		CString csName;

		if(pDoc->m_nType == 0)	// digitech pro
		{
			nProg = theApp.m_nSelProgramDP;
			nLang = theApp.m_nSelLangDP;
			csName = theApp.m_csSelProgramDP;
		}
		else if(pDoc->m_nType == 1)	// mantax computer
		{
			nProg = theApp.m_nSelProgramMC;
			nLang = theApp.m_nSelLangMC;
			csName = theApp.m_csSelProgramMC;
		}

		// check if we have changed program
		if(m_nSelProgram == nProg && m_nSelLang == nLang) return;
		m_nSelProgram = nProg;
		m_nSelLang = nLang;


		// remove files in the reportcontrol
		m_caFiles.RemoveAll();
		m_wndReport.GetRecords()->RemoveAll();
		m_wndReport.Populate();



		PROGRAM prg;
		KEY key;
		LANGUAGE lng;
		FILES file;

		prg = theApp.m_caPrograms.GetAt(nProg);
		lng = theApp.m_caLanguages.GetAt(nLang);

		m_csName = prg.csName;
		m_csID.Format(_T("%d"), prg.nID);
		m_nType = prg.nProgtype;
		m_csVersion.Format(_T("%d.%d"), lng.nVersion/10, lng.nVersion%10);
		m_csDescription = lng.csDesc;
		m_csLang = m_cLocale.GetLangString(lng.csLang);
		m_nType = theApp.m_nType;
		if(prg.nLicense > 0) m_bNeedLicense = TRUE;
		else m_bNeedLicense = FALSE;


		// change language selection
		int nLoop=0, nFound = 0;

		CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE);
		do
		{
			csBuf = m_cLocale.GetLangString(nLoop);
			if(csBuf == m_csLang)
			{
				nFound = nLoop;
				break;
			}

			nLoop++;
		}
		while(csBuf != _T(""));
		m_nLanguage = nFound;



		// get the files from the package
		csBuf = lng.csPath.Right(4);
		if(csBuf.CompareNoCase(_T(".hmi")) == 0)
		{
			m_bNew = FALSE;
			CString csDestpath;
			csDestpath = theApp.m_csLocalPath;

			g_pZIP->Open(csDestpath /*+ _T("\\")*/ + lng.csPath);

			// extract "package.xml" and inspect it
			OpenPackage();

			g_pZIP->Close();
		}

		UpdateData(FALSE);
		
		// always add an empty, final, line
		m_wndReport.AddRecord(new CKeyRecord("", 0));
		m_wndReport.Populate();
		m_wndReport.AllowEdit(TRUE);
		m_wndReport.FocusSubItems(TRUE);
	}


//	UpdateData(FALSE);
}

// save the package
void CProgramDlg::OnSave()
{
	UpdateData(TRUE);

	if(m_csID == "" || m_csName == "" || m_csVersion == "" || m_nLanguage == -1)
	{
		m_bDontBother = TRUE;
		AfxMessageBox(_T("Du har inte fyllt i tillr�ckligt med information!"));
		m_bDontBother = FALSE;
		return;
	}
	
	pDoc->m_bNew = FALSE;
	m_bNew = FALSE;


	// make the package and save the info
	CreatePackageXML();


	// open the info-dialog
	// the programsdlg should be updated aswell
	CDocTemplate *pTemplate;
	CWinApp* pApp = AfxGetApp();
	CString csDocName, csResStr, csDocTitle;
	csResStr.LoadString(IDR_PROGRAMS);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(csDocName, CDocTemplate::docName);
		ASSERT(pTemplate != NULL);
		csDocName = '\n' + csDocName;

		if (pTemplate && csDocName.Compare(csResStr) == 0)
		{
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			while(posDOC != NULL)
			{
				CMDISampleSuiteDoc* pDocument = (CMDISampleSuiteDoc*)pTemplate->GetNextDoc(posDOC);
				POSITION pos = pDocument->GetFirstViewPosition();
				if(pos != NULL && pDocument->m_nType == pDoc->m_nType)
				{
					CProgramsDlg* pView = (CProgramsDlg*)pDocument->GetNextView(pos);

					PROGRAM prg;
					LANGUAGE lng;
					int nStartIndex=0, nID, nProgIndex=0, nLangIndex=0;
					BOOL bFoundPrg = FALSE, bFoundLng = FALSE;;

					nID = atoi(m_csID);

					for(int nLoop=0; nLoop<theApp.m_caPrograms.GetSize(); nLoop++)
					{
						prg = theApp.m_caPrograms.GetAt(nLoop);

						if(prg.csName == m_csName && prg.nID == nID && prg.nProgtype == m_nType)
						{
							bFoundPrg = TRUE;
							nProgIndex = nLoop;
							break;
						}

						nStartIndex += prg.nLangs;
					}

					((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
					m_csLang = m_cLocale.GetLangAbbr(m_csLang);

					// save the current data
//					if(m_bNew == TRUE)	// new
//					{
						if(bFoundPrg == TRUE)
						{
							// update existing program
							for(nLoop=0; nLoop<prg.nLangs; nLoop++)
							{
								lng = theApp.m_caLanguages.GetAt(nStartIndex + nLoop);

								if(lng.csLang == m_csLang)
								{
									bFoundLng = TRUE;
									break;
								}
							}
							nLangIndex = nStartIndex + nLoop;

							if(bFoundLng == TRUE)
							{
								// update the existing language
								lng.csLang = m_csLang;
								lng.csDesc = m_csDescription;
								lng.nVersion = atof(m_csVersion) * 10.0;
								lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
								theApp.m_caLanguages.SetAt(nLangIndex, lng);

								if(m_bNeedLicense == TRUE) prg.nLicense = 1;
								else prg.nLicense = 0;
								theApp.m_caPrograms.SetAt(nProgIndex, prg);
							}
							else
							{
								// add new language
								lng.csLang = m_csLang;
								lng.csDesc = m_csDescription;
								lng.nVersion = atof(m_csVersion) * 10.0;
								lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
								theApp.m_caLanguages.InsertAt(nLangIndex, lng);

								prg.nLangs++;
								theApp.m_caPrograms.SetAt(nProgIndex, prg);
							}
						}
						else
						{
							// add new language
							lng.csLang = m_csLang;
							lng.csDesc = m_csDescription;
							lng.nVersion = atof(m_csVersion) * 10.0;
							lng.csPath.Format(_T("%s%s.hmi"), m_csID,  lng.csLang.MakeUpper());
							int nLngIndex = theApp.m_caLanguages.Add(lng);

							// add new program
							prg.csName = m_csName;
							prg.nLangs = 1;
							prg.nID = atoi(m_csID);
							prg.nKeys = 0;
							prg.nProgtype = m_nType;
							if(m_bNeedLicense == TRUE) prg.nLicense = 1;
							else prg.nLicense = 0;

							int nPrgIndex = theApp.m_caPrograms.Add(prg);

							theApp.m_nType = m_nType;
							if(m_nType == 0)
							{
								theApp.m_nSelLangDP = nLngIndex;
								theApp.m_nSelProgramDP = nPrgIndex;
							}
							else
							{
								theApp.m_nSelLangMC = nLngIndex;
								theApp.m_nSelProgramMC = nPrgIndex;
							}
						}
//					}
//					else	// old
//					{
//					}


					if(pView->SaveXML() == 0)
						pView->AddSampleRecords();

					posDOC = (POSITION)1;
					break;
				}
			}
		}
	}

}

void CProgramDlg::OnDropFiles(HDROP hDropInfo) 
{
	// TODO: Add your message handler code here and/or call default
	UINT nFiles = DragQueryFile(hDropInfo, (UINT)-1, NULL, 0);

	TCHAR szFileName[_MAX_PATH + 1];
	UINT nNames;
	for(nNames = 0; nNames < nFiles; nNames++)
	{
		::ZeroMemory(szFileName, _MAX_PATH + 1);
		::DragQueryFile(hDropInfo, nNames, (LPTSTR)szFileName, _MAX_PATH + 1);

		AfxMessageBox(szFileName);
//		AfxGetApp()->OpenDocumentFile(szFileName);
	}

	CView::OnDropFiles(hDropInfo);
}

int CProgramDlg::OpenPackage()
{
	int nIndex = -1, nType=0;
	nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

	// we got a package.xml?
	if(nIndex != -1)
	{
		g_pZIP->ExtractFile(nIndex, theApp.m_csTemppath);

		// parse the package xml-file
		XMLResults pResults;
		XMLNode xNode = XMLNode::openFileHelper(theApp.m_csTemppath + _T("package.xml") ,"PMML");

		if(xNode.isEmpty() != 1)
		{
			XMLNode xChild;
			CString csBuf, csFile;
			int i, iterator=0;
			BOOL bDoc = FALSE;

			csBuf = xNode.getAttribute("id");
//			if(csBuf != _T("")) prg.nID = atoi(csBuf);

			csBuf = xNode.getAttribute("name");
//			if(csBuf != _T("")) prg.csName = csBuf;

			csBuf = xNode.getAttribute("lang");
//			if(csBuf != _T("")) lng.csLang = csBuf;

			csBuf = xNode.getAttribute("version");
//			if(csBuf != _T("")) lng.nVersionWeb = (int(atof(csBuf) * 10.0));

			csBuf = xNode.getAttribute("description");
//			if(csBuf != _T("")) lng.csDesc = csBuf;


			int n = xNode.nChildNode("file");
			for(i=0; i<n; i++)
			{
				xChild = xNode.getChildNode("file", &iterator);

				// extract the files found in the xml file.
				nIndex = -1;
				csFile = xChild.getAttribute("name");
				nIndex = g_pZIP->FindFile(csFile, CZipArchive::ffNoCaseSens, true);

				if(nIndex != -1)
				{
					csBuf = xChild.getAttribute("type");
					if(csBuf == _T("DP")) nType = 1;
					else if(csBuf == _T("DPDOC")) nType = 2;

					m_wndReport.AddRecord(new CKeyRecord(csFile, nType));
					m_wndReport.Populate();
				}

			}
		}

		// delete the xml-file
		DeleteFile(theApp.m_csTemppath + _T("package.xml"));
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

void CProgramDlg::OnReportFilebutton(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify != NULL);

	UpdateData(TRUE);

	if(m_csID == "" || m_csName == "" || m_csVersion == "" || m_nLanguage == -1)
	{
		m_bDontBother = TRUE;
		AfxMessageBox(_T("Du har inte fyllt i tillr�ckligt med information!"));
		m_bDontBother = FALSE;
		return;
	}

	if(pItemNotify->pItem->GetItemData() == 0)
	{
		m_bDontBother = TRUE;
		CFileDialog dlg(TRUE, NULL, "", OFN_EXPLORER);
		if(dlg.DoModal() == IDOK)
		{
			int nProg, nLang;
			CString csName;
			CString csDestpath;
			CString csPath;
			csDestpath = theApp.m_csLocalPath;

			if(pDoc->m_nType == 0)	// digitech pro
			{
				nProg = theApp.m_nSelProgramDP;
				nLang = theApp.m_nSelLangDP;
				csName = theApp.m_csSelProgramDP;
			}
			else if(pDoc->m_nType == 1)	// mantax computer
			{
				nProg = theApp.m_nSelProgramMC;
				nLang = theApp.m_nSelLangMC;
				csName = theApp.m_csSelProgramMC;
			}

			LANGUAGE lng;

			if(pDoc->m_bNew == FALSE)
			{
				lng = theApp.m_caLanguages.GetAt(nLang);
				csPath = lng.csPath;
			}
			else
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
				m_csLang = m_cLocale.GetLangAbbr(m_csLang);
				csPath.Format(_T("%s%s.hmi"), m_csID,  m_csLang.MakeUpper());
			}

			// open the package
			CFile fh;
			if(fh.Open(csDestpath + _T("\\") + csPath, CFile::modeRead) != 0)
			{
				fh.Close();
				g_pZIP->Open(csDestpath /*+ _T("\\")*/ + csPath);
			}
			else
			{
				g_pZIP->Open(csDestpath /*+ _T("\\")*/ + csPath, CZipArchive::zipCreate);
			}

			// delete any old file in this index from the package
			if(pItemNotify->pItem->GetCaption(pItemNotify->pColumn) != "")
			{
				int nIndex = -1;
				nIndex = g_pZIP->FindFile(pItemNotify->pItem->GetCaption(pItemNotify->pColumn), CZipArchive::ffNoCaseSens, true);

				if(nIndex != -1)
				{
					g_pZIP->DeleteFile(nIndex);
				}
			}
			else	// add a new line to the report
			{
				m_wndReport.AddRecord(new CKeyRecord("", 0));
				m_wndReport.Populate();
			}

			// add the new file to the package
			pItemNotify->pItem->SetCaption(dlg.GetFileName());
			g_pZIP->AddNewFile(dlg.GetPathName(), dlg.GetFileName());

			// close the package
			g_pZIP->Close();

			// save the whole lot
			m_bDontBother = FALSE;
//			OnSave();
		}
	}
}

int CProgramDlg::CreatePackageXML()
{
	CFile fh;
	char szBuf[1024];

	if(fh.Open(theApp.m_csTemppath + _T("package.xml"), CFile::modeCreate|CFile::modeWrite) != 0)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		CString csLang = m_cLocale.GetLangAbbr(m_csLang);

		sprintf(szBuf, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\r\n");
		fh.Write(szBuf, strlen(szBuf));

		sprintf(szBuf, "\t<package id=\"%s\" name=\"%s\" lang=\"%s\" version=\"%s\" description=\"%s\">\r\n",
			m_csID, m_csName, csLang, m_csVersion, m_csDescription);
		fh.Write(szBuf, strlen(szBuf));

		// get the files from the report
		CXTPReportRecords* pRecords = m_wndReport.GetRecords();
		CXTPReportRecord* pRecord;
		CString csName, csType;

		for(int nLoop=0; nLoop<pRecords->GetCount(); nLoop++)
		{
			pRecord = pRecords->GetAt(nLoop);

			csName = pRecord->GetItem(0)->GetCaption(0);
			csType = pRecord->GetItem(1)->GetCaption(0);

			if(csName != "" && csType != "")
			{
				sprintf(szBuf, "\t\t<file name=\"%s\" type=\"%s\"/>\r\n", csName, csType);
				fh.Write(szBuf, strlen(szBuf));
			}
		}

		sprintf(szBuf, "\t</package>\r\n");
		fh.Write(szBuf, strlen(szBuf));

		fh.Close();


		// update the package aswell
		CString csDestpath = theApp.m_csLocalPath;
		CString csPath;
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		m_csLang = m_cLocale.GetLangAbbr(m_csLang);
		csPath.Format(_T("%s%s.hmi"), m_csID,  m_csLang);

		// open the package
		if(fh.Open(csDestpath + csPath, CFile::modeRead) == 0)
		{
			// create a new archive
			g_pZIP->Open(csDestpath + csPath, CZipArchive::zipCreate);
		}
		else
		{
			// open existing archive
			fh.Close();
			g_pZIP->Open(csDestpath + csPath);
		}


		int nIndex = -1;
		nIndex = g_pZIP->FindFile(_T("package.xml"), CZipArchive::ffNoCaseSens, true);

		if(nIndex != -1)
		{
			g_pZIP->DeleteFile(nIndex);
		}

		g_pZIP->AddNewFile(theApp.m_csTemppath + _T("package.xml"), _T("package.xml"));

		DeleteFile(theApp.m_csTemppath + _T("package.xml"));

		g_pZIP->Close();
	}
	else
		return FALSE;

	return TRUE;
}

// turn on/off the trashcan-icon
void CProgramDlg::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CXTPReportRecord* pRecord = m_wndReport.GetFocusedRow()->GetRecord();
	if (pRecord)
	{
		CString csBuf = pRecord->GetItem(0)->GetCaption(0);

		if(csBuf != _T(""))
		{
			// lit the trashcan
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
		}
		else
		{
			// disable trashcan
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
		}
	}
}

// delete selected program from the package
void CProgramDlg::OnDelete()
{
	CXTPReportRecord* pRecord = m_wndReport.GetFocusedRow()->GetRecord();
	if (pRecord)
	{
		CString csBuf = pRecord->GetItem(0)->GetCaption(0);	// filename

		// open archive
		((CComboBox*)GetDlgItem(IDC_COMBO_LANGUAGE))->GetLBText(m_nLanguage, m_csLang);
		CString csLang = m_cLocale.GetLangAbbr(m_csLang);
		CString csPath;
		csPath.Format(_T("%s%s.hmi"), m_csID,  csLang);

		g_pZIP->Open(theApp.m_csLocalPath + csPath);

		int nIndex = g_pZIP->FindFile(csBuf, CZipArchive::ffNoCaseSens, true);

		if(nIndex != -1)
		{
			g_pZIP->DeleteFile(nIndex);

			// update the report aswell
			nIndex = pRecord->GetIndex();
			m_wndReport.GetRecords()->RemoveAt(nIndex);
			m_wndReport.Populate();
		}

		g_pZIP->Close();
	}
}
