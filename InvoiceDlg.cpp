// InvoiceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "InvoiceDlg.h"
#include "samplesuite.h"

// CInvoiceDlg

IMPLEMENT_DYNCREATE(CInvoiceDlg, CFormView)

CInvoiceDlg::CInvoiceDlg()
	: CFormView(CInvoiceDlg::IDD)
	, m_bInvoice(FALSE)
	, m_csCustNum(_T(""))
	, m_csName(_T(""))
	, m_csCompany(_T(""))
	, m_csVat(_T(""))
	, m_csAddress1(_T(""))
	, m_csAddress2(_T(""))
	, m_csAddress3(_T(""))
	, m_csCountry(_T(""))
	, m_csTelephone(_T(""))
	, m_csEmail(_T(""))
{

}

CInvoiceDlg::~CInvoiceDlg()
{
}

void CInvoiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_INVOICE, m_bInvoice);
	DDX_Text(pDX, IDC_EDIT_CUSTNUM2, m_csCustNum);
	DDX_Text(pDX, IDC_EDIT_NAME2, m_csName);
	DDX_Text(pDX, IDC_EDIT_COMPANY2, m_csCompany);
	DDX_Text(pDX, IDC_EDIT_VAT2, m_csVat);
	DDX_Text(pDX, IDC_EDIT_ADDRESS4, m_csAddress1);
	DDX_Text(pDX, IDC_EDIT_ADDRESS5, m_csAddress2);
	DDX_Text(pDX, IDC_EDIT_ADDRESS6, m_csAddress3);
	DDX_Text(pDX, IDC_EDIT_COUNTRY2, m_csCountry);
	DDX_Text(pDX, IDC_EDIT_TELEPHONE2, m_csTelephone);
	DDX_Text(pDX, IDC_EDIT_EMAIL2, m_csEmail);
}

BEGIN_MESSAGE_MAP(CInvoiceDlg, CFormView)
	ON_BN_CLICKED(IDC_CHECK_INVOICE, OnBnClickedCheckInvoice)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CInvoiceDlg diagnostics

#ifdef _DEBUG
void CInvoiceDlg::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CInvoiceDlg::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CInvoiceDlg message handlers
void CInvoiceDlg::OnInitialUpdate()
{
	m_bInvoice = theApp.m_bInUse;
	m_csCustNum = theApp.m_csInCustNum;
	m_csName = theApp.m_csInName;
	m_csCompany = theApp.m_csInCompany;
	m_csVat = theApp.m_csInVat;
	m_csAddress1 = theApp.m_csInAddress1;
	m_csAddress2 = theApp.m_csInAddress2;
	m_csAddress3 = theApp.m_csInAddress3;
	m_csCountry = theApp.m_csInCountry;
	m_csTelephone = theApp.m_csInTelephone;
	m_csEmail = theApp.m_csInEmail;

	CFormView::OnInitialUpdate();

	SetDlgItemText(IDC_CHECK_INVOICE, g_pXML->str(25));
	SetDlgItemText(IDC_STATIC_CUSTNUM2, g_pXML->str(26));
	SetDlgItemText(IDC_STATIC_NAME2, g_pXML->str(27));
	SetDlgItemText(IDC_STATIC_COMPANY2, g_pXML->str(28));
	SetDlgItemText(IDC_STATIC_VAT2, g_pXML->str(29));
	SetDlgItemText(IDC_STATIC_ADDRESS2, g_pXML->str(30));
	SetDlgItemText(IDC_STATIC_COUNTRY2, g_pXML->str(31));
	SetDlgItemText(IDC_STATIC_TELEPHONE2, g_pXML->str(32));
	SetDlgItemText(IDC_STATIC_EMAIL2, g_pXML->str(33));

	OnBnClickedCheckInvoice();
}


void CInvoiceDlg::OnBnClickedCheckInvoice()
{
	UpdateData(TRUE);

	// ghost/unghost the controls in this view
	GetDlgItem(IDC_EDIT_CUSTNUM2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_NAME2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_COMPANY2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_VAT2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_ADDRESS4)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_ADDRESS5)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_ADDRESS6)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_COUNTRY2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_TELEPHONE2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_EDIT_EMAIL2)->EnableWindow(m_bInvoice);

	GetDlgItem(IDC_STATIC_CUSTNUM2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_NAME2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_COMPANY2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_VAT2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_ADDRESS2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_COUNTRY2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_TELEPHONE2)->EnableWindow(m_bInvoice);
	GetDlgItem(IDC_STATIC_EMAIL2)->EnableWindow(m_bInvoice);
}

void CInvoiceDlg::OnClose()
{
	CFormView::OnClose();
}

void CInvoiceDlg::OnDestroy()
{
	UpdateData(TRUE);

	theApp.m_bInUse = m_bInvoice;
	theApp.m_csInCustNum = m_csCustNum;
	theApp.m_csInName = m_csName;
	theApp.m_csInCompany = m_csCompany;
	theApp.m_csInVat = m_csVat;
	theApp.m_csInAddress1 = m_csAddress1;
	theApp.m_csInAddress2 = m_csAddress2;
	theApp.m_csInAddress3 = m_csAddress3;
	theApp.m_csInCountry = m_csCountry;
	theApp.m_csInTelephone = m_csTelephone;
	theApp.m_csInEmail = m_csEmail;

	CFormView::OnDestroy();
}
