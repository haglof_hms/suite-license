// LicenseDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LicenseDlg.h"
#include "Samplesuite.h"
#include "XBrowseForFolder.h"

// CLicenseDlg dialog

IMPLEMENT_DYNAMIC(CLicenseDlg, CDialog)

CLicenseDlg::CLicenseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLicenseDlg::IDD, pParent)
{
	m_csPath = _T("");
}

CLicenseDlg::~CLicenseDlg()
{
}

void CLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_csPath);
}


BEGIN_MESSAGE_MAP(CLicenseDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_CHOOSE_PATH, OnBnClickedButtonChoosePath)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_EN_CHANGE(IDC_EDIT_PATH, OnEnChangeEditPath)
END_MESSAGE_MAP()


BOOL CLicenseDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(g_pXML->str(200));
	SetDlgItemText(IDC_STATIC_INFO, g_pXML->str(201));
	SetDlgItemText(IDC_STATIC_PATH, g_pXML->str(200));
	SetDlgItemText(IDOK, g_pXML->str(62));
	SetDlgItemText(IDCANCEL, g_pXML->str(63));

	OnEnChangeEditPath();

	return TRUE;
}

void CLicenseDlg::OnEnChangeEditPath()
{
	UpdateData(TRUE);
	if(m_csPath == _T(""))
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	else
		GetDlgItem(IDOK)->EnableWindow(TRUE);
}

// CLicenseDlg message handlers
// choose path for license-file
void CLicenseDlg::OnBnClickedButtonChoosePath()
{
	// show a filechooser
	UpdateData(TRUE);

	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(this->GetSafeHwnd(),
		m_csPath,
		szFolder,
		sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		m_csPath = szFolder;
		if(m_csPath.GetAt(m_csPath.GetLength()-1) != '\\')
			m_csPath += _T("\\");
		UpdateData(FALSE);

		OnEnChangeEditPath();
	}
}

void CLicenseDlg::OnBnClickedOk()
{
	UpdateData(FALSE);
	CDialog::OnOK();
}
