#pragma once

#ifndef _ProgramsDlg_H_
#define _ProgramsDlg_H_


#include "Resource.h"
#include <afxcmn.h>
#include <afxwin.h>
#include "Locale.h"
#include <XTToolkitPro.h>
#include "SampleSuite.h"
#include "InvoiceDlg.h"

/*------------------------------------------------------------
  Document class
------------------------------------------------------------*/
class CLicenseDoc : public CDocument
{
protected: // create from serialization only
	CLicenseDoc();
	DECLARE_DYNCREATE(CLicenseDoc)

// Attributes
public:
	// DATA

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseDoc)
	public:
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL
	virtual void OnCloseDocument();

// Implementation
public:
	virtual ~CLicenseDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLicenseDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/*------------------------------------------------------------
  Frame class
------------------------------------------------------------*/
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CLicenseFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CLicenseFrame)

	BOOL m_bKeys;
	BOOL m_bOnce;

// pane stuff
	CXTPDockingPaneManager m_paneManager;

public:
	CLicenseFrame();
	CInvoiceDlg* m_pViewOne;
//	CMap<UINT,UINT, CWnd* , CWnd*> m_mapPanes;
	

	// Toolbar
	CXTPToolBar m_wndToolBar;
	CStatusBar  m_wndStatusBar;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLicenseFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
	//{{AFX_MSG(CLicenseFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
protected:
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	afx_msg void OnPaint();

public:
	afx_msg void OnClose();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	void SetButtonEnabled(UINT nID, BOOL bEnabled);
	void OnBnClickedLicense();
	void OnBnClickedOrder();
	void OnBnClickedPath();

	void OnBnClickedSendLicense();
	void OnBnClickedGetLicense();

};


/*------------------------------------------------------------
  Dialog (view) class
------------------------------------------------------------*/
class CLicenseView : public CView //CXTPReportView 
{
	DECLARE_DYNCREATE(CLicenseView)
protected: // create from serialization only
	CLicenseView();
	virtual ~CLicenseView();

// Attributes
public:
	//{{AFX_DATA(CLicenseView)
	enum { IDD = IDD_LICENSE };
	CLicenseDoc* pDoc;
	//}}AFX_DATA

private:
	CXTPTabControl m_wndTabControl;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	void UpdateDocTitle();

// Operations
public:

private:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLicenseView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
public:
	virtual void OnInitialUpdate();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//{{AFX_MSG(CLicenseView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnUpdate(CView *pSender, LPARAM lHint, CObject *pHint);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);

	DECLARE_MESSAGE_MAP()
};
#endif
