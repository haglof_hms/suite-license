// SampleSuite.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "SampleSuite.h"
#include "LicenseView.h"
#include "ContactFormView.h"
#include "XBrowseForFolder.h"
#include "LicenseDlg.h"
#include <iostream>
using namespace std;

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines

CString GetLocalComputerName() { 
  TCHAR chrComputerName[MAX_COMPUTERNAME_LENGTH + 1]; 
  CString strRetVal; 
  DWORD dwBufferSize = MAX_COMPUTERNAME_LENGTH + 1; 
  
  if(GetComputerName(chrComputerName,&dwBufferSize)) { 
    // We got the name, set the return value. 
    strRetVal = chrComputerName; 
  } else { 
    // Failed to get the name, call GetLastError here to get 
    // the error code. 
    strRetVal = ""; 
  } 

  return(strRetVal); 
}

// globals
std::vector<HINSTANCE> m_vecHInstTable;
RLFReader* g_pXML;
static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;
static AFX_EXTENSION_MODULE SampleSuiteDLL = { NULL, NULL };

CLicProtectorEasyGo270 proxyLicProtector;	// Licence Protector class
CString sLicFile;
CString sPSK;
CString	sPSK2 = "567890";
CString g_csLangFN;
#define INFO_BUFFER_SIZE 32767


CSampleSuite theApp;

CSampleSuite::CSampleSuite():
m_hCheckThread(NULL)
{
	TCHAR szPath[MAX_PATH];
	GetTempPath(MAX_PATH, szPath);
	m_csTempPath = szPath;

	theApp.m_bLicRefreshed = FALSE;

	CString csBuf = regGetStr_LM(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), _T(""));
	if(csBuf == _T(""))
	{
		m_csLicfilePath = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), _T(""));
	}
	else
	{
		m_csLicfilePath = csBuf;
		regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), m_csLicfilePath);
	}

	if(m_csLicfilePath.GetLength() > 0)
	{
		if(m_csLicfilePath.GetAt(m_csLicfilePath.GetLength()-1) != '\\')
			m_csLicfilePath += _T("\\");
	}

	m_bLicfile = FALSE;

	m_csCustNum = _T("");
	m_csName = _T("");
	m_csCompany = _T("");
	m_csVat = _T("");
	m_csAddress1 = _T("");
	m_csAddress2 = _T("");
	m_csAddress3 = _T("");
	m_csCountry = _T("");
	m_csTelephone = _T("");
	m_csEmail = _T("");
	m_csNotes = _T("");
	m_bInUse = FALSE;
	m_csInCustNum = _T("");
	m_csInName = _T("");
	m_csInCompany = _T("");
	m_csInVat = _T("");
	m_csInAddress1 = _T("");
	m_csInAddress2 = _T("");
	m_csInAddress3 = _T("");
	m_csInCountry = _T("");
	m_csInTelephone = _T("");
	m_csInEmail = _T("");


	// contact information
	m_csCustNum = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("CustNum"), _T(""));
	m_csName = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Name"), _T(""));
	m_csCompany = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Company"), _T(""));
	m_csVat = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("VAT"), _T(""));
	m_csAddress1 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address1"), _T(""));
	m_csAddress2 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address2"), _T(""));
	m_csAddress3 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address3"), _T(""));
	m_csCountry = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Country"), _T(""));
	m_csTelephone = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Telephone"), _T(""));
	m_csEmail = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Email"), _T(""));
	m_csNotes = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Notes"), _T(""));

	m_bInUse = regGetInt(REG_ROOT, _T("HMS_License\\Settings"), _T("InUse"), FALSE);
	m_csInCustNum = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCustNum"), _T(""));
	m_csInName = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InName"), _T(""));
	m_csInCompany = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCompany"), _T(""));
	m_csInVat = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InVAT"), _T(""));
	m_csInAddress1 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress1"), _T(""));
	m_csInAddress2 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress2"), _T(""));
	m_csInAddress3 = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress3"), _T(""));
	m_csInCountry = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCountry"), _T(""));
	m_csInTelephone = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InTelephone"), _T(""));
	m_csInEmail = regGetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InEmail"), _T(""));

	m_csEmailTo = regGetStr_LM(REG_ROOT, _T("HMS_License\\Settings"), _T("EmailTo"), _T(""));
	m_csEmailSub = regGetStr_LM(REG_ROOT, _T("HMS_License\\Settings"), _T("EmailSub"), _T(""));


	// Create the automation object
	BOOL bRes = proxyLicProtector.CreateDispatch(_T("LicProtectorEasyGo.LicProtectorEasyGo270"));
	if(!bRes)
	{
		AfxMessageBox(_T("License.dll: Automation object not found!"), MB_OK|MB_ICONERROR);
		return;
	}

	// start a thread that looks if it can connect to license file
	m_hCheckThread = CreateThread(NULL, 0, CheckRefresh, (LPVOID)this, 0, NULL);

	m_csComputername = GetLocalComputerName();

	TCHAR infoBuf[1234];
	DWORD bufCharCount = 1234;
	GetUserName( infoBuf, &bufCharCount );
	m_csUsername = infoBuf;

}

CSampleSuite::~CSampleSuite()
{
	// Kill licence check thread
	if( m_hCheckThread )
	{
		CloseHandle(m_hCheckThread);
		m_hCheckThread = NULL;
	}

	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), m_csLicfilePath);

	// contact information
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("CustNum"), m_csCustNum);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Name"), m_csName);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Company"), m_csCompany);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("VAT"), m_csVat);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address1"), m_csAddress1);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address2"), m_csAddress2);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Address3"), m_csAddress3);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Country"), m_csCountry);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Telephone"), m_csTelephone);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Email"), m_csEmail);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("Notes"), m_csNotes);

	regSetInt(REG_ROOT, _T("HMS_License\\Settings"), _T("InUse"), m_bInUse);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCustNum"), m_csInCustNum);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InName"), m_csInName);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCompany"), m_csInCompany);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InVAT"), m_csInVat);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress1"), m_csInAddress1);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress2"), m_csInAddress2);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InAddress3"), m_csInAddress3);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InCountry"), m_csInCountry);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InTelephone"), m_csInTelephone);
	regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("InEmail"), m_csInEmail);

//	regSetStr_LM(REG_ROOT, _T("HMS_License\\Settings"), _T("EmailTo"), m_csEmailTo);
//	regSetStr_LM(REG_ROOT, _T("HMS_License\\Settings"), _T("EmailSub"), m_csEmailSub);
}

int CSampleSuite::OpenLicense()
{
	CString csLicFile;
	csLicFile.Format(_T("%sHaglofManagementSystems.lic"), m_csLicfilePath);
	sPSK = "1234";

	m_caInstCodes.RemoveAll();
	m_caLicenses.RemoveAll();

	if(m_csLicfilePath == _T(""))
	{
		m_bLicfile = FALSE;
		return 0;
	}

	CFile cfLic;
	if( cfLic.Open(csLicFile, CFile::modeRead) == 0)
	{
		WriteToLog("Copy default license file");

		// license file does not exist, try to copy a new one into m_csLicfilePath
		CString csTmp;
		csTmp.Format(_T("%sSetup\\HaglofManagementSystems.lic"), getProgDir());

		// open default license file first, so any demo-timers is activated
		proxyLicProtector.put_SecurityLevel(0);
		long lRes = proxyLicProtector.Prepare(csTmp, sPSK + sPSK2);

		int nCurPos = 0;
		CString csBuf, csRes, csType;
		long lValidate;
		csBuf = proxyLicProtector.GetModuleList(false, true);
		csRes = csBuf.Tokenize(_T(";"), nCurPos);
		if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);

		while (csRes != _T("") && csType != _T(""))
		{
			if(csType == _T("Y"))	// YesNo
				lValidate = proxyLicProtector.ValidatesYes(csRes);
			else if(csType == _T("R"))	// Concurrent user
				lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);
			else
				lValidate = proxyLicProtector.Validate(csRes, _T(""), false);

			// get next module
			csRes = csBuf.Tokenize(_T(";"), nCurPos);
			if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);
		}

		lRes = proxyLicProtector.Quit();

		if(!CopyFile(csTmp, csLicFile, TRUE))
		{
			m_bLicfile = FALSE;
			return 0;
		}

		WriteToLog("Copy default license file OK");
	}
	else
	{
		cfLic.Close();
	}
	m_bLicfile = TRUE;

	WriteToLog("Open license file");
	// Call prepare to open the licence file
#ifdef ADVANCED
	proxyLicProtector.put_SecurityLevel(1);
	long nRes = proxyLicProtector.PrepareAdvanced(csLicFile, sPSK + sPSK2);
#else
	proxyLicProtector.put_SecurityLevel(0);
	long nRes = proxyLicProtector.Prepare(csLicFile, sPSK + sPSK2);
#endif
	if(nRes != 0)
	{
		AfxMessageBox(proxyLicProtector.GetErrorMessage(nRes), MB_OK|MB_ICONERROR);
		AfxMessageBox(proxyLicProtector.GetErrorMessage(nRes), MB_OK|MB_ICONERROR);
		m_bLicfile = FALSE;
		return 0;
	}
	WriteToLog("Open license file OK");
	
	// make sure that no one has screwed with the clock or the license file
	if( !CheckTampered() ) return FALSE;


	// Language code (10000 = German, 20000 = English, 30000 = French, 40000 = Italian, 50000 = European Portuguese,
	// 60000 = Brazilian Portuguese, 70000 = Dutch, 80000 = Swedish, 90000 = Spanish, 100000 = Norwegian
	int nLang = 20000;
	CString csLang = getLangSet();

	if(csLang == "DEU") nLang = 10000;
	else if(csLang == "ENU") nLang = 20000;
	else if(csLang == "FRA") nLang = 30000;
	else if(csLang == "ITA") nLang = 40000;
	else if(csLang == "PTG") nLang = 50000;
	else if(csLang == "PTB") nLang = 60000;
	else if(csLang == "NLB" || csLang == "NLD") nLang = 70000;
	else if(csLang == "SVE") nLang = 80000;
	else if(csLang == "ESP" || csLang == "ESN") nLang = 90000;
	else if(csLang == "NOR" || csLang == "NON") nLang = 100000;
	proxyLicProtector.SetLanguage(nLang);

	proxyLicProtector.put_ShowWAStartPage(TRUE);
	proxyLicProtector.put_ShowWAProgressPage(TRUE);
	proxyLicProtector.put_ShowWAResultPage(TRUE);
	
	return 1;
}

int CSampleSuite::GatherLicenses(BOOL bWait)
{
	// License stuff
	CString csBuf, csBuf2;
	LICENSE lic;
	CString sMsg;

	if(m_csLicfilePath == _T("")) return -1;

	// check if we have a license file
	if(m_bLicfile == FALSE)
	{
		// try to open it
		OpenLicense();
	}
	if(m_bLicfile == FALSE) return -1;


	m_caLicenses.RemoveAll();


	// check if the Shell is activated
	CString csLic;

	m_bTampered = FALSE;
	if( proxyLicProtector.get_LicenceTampered() )	// licensefile has been messed with
	{
		m_bTampered = TRUE;
	}

	m_bDateChanged = FALSE;
	if( proxyLicProtector.get_SysdateChanged() )	// date has been changed atleast 1 day backwards
	{
		m_bDateChanged = TRUE;
	}


	long lRes = proxyLicProtector.GetCopyProtection();
	if(lRes == 0)
		m_bActivated = FALSE;
	else
		m_bActivated = TRUE;
	
	// are we on a USB-memory?
	if( proxyLicProtector.GetInstCode(11) != _T("") )
	{
		int nFound = m_csLicfilePath.Find('\\');
		if( m_csLicfilePath.Find('\\', nFound+1) != -1 )
		{
			::MessageBox(0, g_pXML->str(52), g_pXML->str(-999), MB_OK|MB_ICONEXCLAMATION); //_T("License file must be on the root of the USB-memory!"));
			m_bTampered = TRUE;
			return -1;
		}
	}


	if(m_bTampered == FALSE && m_bDateChanged == FALSE)
	{
		// get the modules
		CString csRes, csType, csBuf2;
		int nCurPos = 0;
		long lValidate, lRemDays, lRemLic;
		BOOL bFirst = TRUE;

		WriteToLog("Gather modules");
		csBuf = proxyLicProtector.GetModuleList(false, true);
		csRes = csBuf.Tokenize(_T(";"), nCurPos);
		if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);

		while (csRes != _T("") && csType != _T(""))
		{
			if(csType == _T("U"))	// user
				lic.nType = 1;
			else if(csType == _T("S"))	// computer
				lic.nType = 2;
			else if(csType == _T("R"))	// concurrent user
				lic.nType = 6;
			else if(csType == _T("Y"))	// YesNo
				lic.nType = 4;
			else if(csType == _T("C"))	// counter
				lic.nType = 3;
			else if(csType == _T("I"))	// item counter
				lic.nType = 5;

			proxyLicProtector.SetCheckInterval(csRes, 15);	// set a longer delay between updates

			if(lic.nType == 4)	// YesNo
			{
				lValidate = proxyLicProtector.ValidatesYes(csRes);

				if(lValidate == 0)
				{
					lic.bYesNo = TRUE;
					lic.bDemo = FALSE;
				}
				else if(lValidate == 1)
				{
					lic.bYesNo = TRUE;
					lic.bDemo = TRUE;
				}
				else if(lValidate == 3)
				{
					lic.bYesNo = FALSE;
					lic.bDemo = FALSE;
				}
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
			}
			else if(lic.nType == 6)	// concurrent user
			{
				lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);
				lRemLic = proxyLicProtector.RemainingLicences(csRes);

				proxyLicProtector.SetCheckInterval(csRes, 15);

				if(lValidate == 1 || lValidate == 16 || lValidate == 17)	// demo version
				{
					lic.bDemo = TRUE;
				}
				else if(lValidate == 0)	// no demo
				{
					lic.bDemo = FALSE;
				}
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
				else
				{
					// concurrent users
					if(lValidate == 32)	// no free licenses
					{
						CStringA csBuf;
						csBuf.Format("GatherLicense(): No free concurrent licenses (%s+%s)", m_csComputername, m_csUsername);
						WriteToLog(csBuf);

						long lLicenses = proxyLicProtector.TotalLicences(csRes);
						if(lLicenses == 1)	// we have only 1 license, and its in use
						{
							if(bWait)
							{
								WriteToLog("GatherLicenses(): Sleep and retest license");

								// try if it is a dead client
								proxyLicProtector.RemoveAllItems(csRes);
								long lInterval = proxyLicProtector.CheckInterval(csRes);
								Sleep(lInterval * 2000);
							}

							// test again
							lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);

							if(lValidate == 0)	// it was a dead user
							{
								lic.bDemo = FALSE;
								lRemLic = 0;
							}
							else if(lValidate == 1)	// demo
							{
								lic.bDemo = TRUE;
								lRemLic = 0;
							}
							else if(lValidate == 128)	// can not write to license file
							{
								m_caLicenses.RemoveAll();
								theApp.m_bLicRefreshed = TRUE;

								MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);
								WriteToLog("GatherLicenses(): Can not write to license file");

								return 1;				
							}
							else	// license still in use
							{
								WriteToLog("GatherLicenses(): License still in use");

								lic.bDemo = FALSE;
								lRemLic = -1;
							}
						}
						else
						{
							lic.bDemo = FALSE;
							lRemLic = -1;
						}
					}
				}
			}
			else
			{
				lValidate = proxyLicProtector.Validate(csRes, _T(""), false);

				if(lValidate == 1)	// demo version
					lic.bDemo = TRUE;
				else if(lValidate == 0)	// no demo
					lic.bDemo = FALSE;
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
				else
					lic.bDemo = TRUE;
			}
			
			lic.nNoOfDays = proxyLicProtector.NoOfDays(csRes);
			lic.nWebActivation = proxyLicProtector.GetWebActivation(csRes);
			lic.dtExpires = proxyLicProtector.ExpiredOn(csRes);

			lRemDays = proxyLicProtector.RemainingDays(csRes);
			lic.nRemDays = (int)lRemDays;

			if(lic.nType != 6) lRemLic = proxyLicProtector.RemainingLicences(csRes);
			lic.nRemLic = (int)lRemLic;

			lic.csModID = csRes;

			lic.csModName = proxyLicProtector.ModuleName(lic.csModID);

			lic.csTag = proxyLicProtector.GetTagValue(lic.csModID);

			lic.nLicenses = proxyLicProtector.TotalLicences(lic.csModID);

			m_caLicenses.Add(lic);


			// get next module
			csRes = csBuf.Tokenize(_T(";"), nCurPos);
			if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);
		};
		WriteToLog("Gather modules OK");
	}

	theApp.m_bLicRefreshed = TRUE;

	return 1;
}

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("License.DLL Initializing!\n");
	
		// create the XML-parser class.
		g_pXML = new RLFReader();

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(SampleSuiteDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("License.DLL Terminating!\n");

		// close the license file
		long lRes = proxyLicProtector.Quit();
		if(lRes != 0)
		{
			AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes), MB_OK|MB_ICONERROR);
		}
		proxyLicProtector.DetachDispatch();

		delete g_pXML;

		// Terminate the library before destructors are called
		AfxTermExtensionModule(SampleSuiteDLL);
	}

	hInst = hInstance;

	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void DLL_BUILD InitSuite(CStringArray *user_modules, vecINDEX_TABLE &vecIndex, vecINFO_TABLE &vecInfo)
{
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(SampleSuiteDLL);

	theApp.m_vecInfoTable = &vecInfo;


	CString csModuleFN = getModuleFN(hInst);
	m_vecHInstTable.clear();

	// Setup the language filename
	g_csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	// load the language file
	CString csLang = getLangSet();
	g_pXML->Load(g_csLangFN + csLang + _T(".xml"));

	theApp.OpenLicense();
	if(theApp.m_bLicfile == TRUE)
	{
		theApp.GatherLicenses(FALSE);	// dont do deep wait
		theApp.UpdateLicensefile();
	}

	CWinApp* pApp = AfxGetApp();

	// Programs
	pApp->AddDocTemplate(new CMultiDocTemplate(IDR_LICENSE, 
			RUNTIME_CLASS(CLicenseDoc),
			RUNTIME_CLASS(CLicenseFrame),
			RUNTIME_CLASS(CContactFormView)));	//(CLicenseView)));
	vecIndex.push_back(INDEX_TABLE(IDR_LICENSE, csModuleFN, g_csLangFN, TRUE));

	// Get version information; 060803 p�d
	CString csVersion, csCopyright, csCompany;

	csVersion	= getVersionInfo(hInst, VER_NUMBER);
	csCopyright	= getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany	= getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		1, //Set to 1 to indicate a SUITE; 2 indicates a  User Module,
		g_csLangFN,
		csVersion,
		csCopyright,
		csCompany));


	// *****************************************************************************
	//	Load user module(s), specified in the ShellTree data file for this SUITE
	// ******************************************************************************
	typedef CRuntimeClass *(*Func)(CWinApp *, LPCTSTR suite, vecINDEX_TABLE &, vecINFO_TABLE &);
	Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"), getModulesDir(), user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
					USES_CONVERSION;
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], "InitModule" );

					if (proc != NULL)
					{
						// call the function
						proc(pApp, csModuleFN, vecIndex, vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)
}

void DLL_BUILD OpenSuite(int idx, LPCTSTR func, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sLangFN;
	CString sVecIndexTableModuleFN;
	CString sCaption;
	int nTableIndex;
	BOOL bFound = FALSE, bIsOneInst = FALSE;
	CString csPath;


	CWinApp* pApp = AfxGetApp();
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() > 0)
	{
		for (UINT i = 0;i < vecIndex.size();i++)
		{
			nTableIndex = vecIndex[i].nTableIndex;
			sVecIndexTableModuleFN = vecIndex[i].szSuite;

			if (nTableIndex == idx && sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
			{
				// Get the stringtable resource, matching the TableIndex
				// This string is compared to the title of the document; 051212 p�d
				sResStr.LoadString(nTableIndex);

				// Get filename including searchpath to THIS SUITE, as set in
				// the table index vector, for suites and module(s); 051213 p�d
				sVecIndexTableModuleFN = vecIndex[i].szSuite;

				// Need to setup the Actual filename here, because we need to 
				// get the Language set in registry, on Openning a View; 051214 p�d
				sLangFN = vecIndex[i].szLanguageFN;

				bIsOneInst = vecIndex[i].bOneInstance;

				if (g_pXML->Load(sLangFN))
				{
					sCaption = g_pXML->str(nTableIndex);
				}
				else
				{
					AfxMessageBox(_T("Could not open languagefile!"), MB_ICONERROR);
				}

				// Check if the document or module is in this SUITE; 051213 p�d
				if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				{
					POSITION pos = pApp->GetFirstDocTemplatePosition();
					while(pos != NULL)
					{
						pTemplate = pApp->GetNextDocTemplate(pos);
						pTemplate->GetDocString(sDocName, CDocTemplate::docName);
						ASSERT(pTemplate != NULL);
						// Need to add a linefeed, infront of the docName.
						// This is because, for some reason, the document title,
						// set in resource, must have a linefeed.
						// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
						sDocName = '\n' + sDocName;

						if (pTemplate && sDocName.Compare(sResStr) == 0)
						{
							if(bIsOneInst == TRUE)
							{
								// Find the CDocument for this tamplate, and set title.
								// Title is set in Languagefile; OBS! The nTableIndex
								// matches the string id in the languagefile; 051129 p�d
								POSITION posDOC = pTemplate->GetFirstDocPosition();
								while(posDOC != NULL)
								{
									CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
									POSITION pos = pDocument->GetFirstViewPosition();
									if(pos != NULL)
									{
										CView* pView = pDocument->GetNextView(pos);
										pView->GetParent()->BringWindowToTop();
										pView->GetParent()->SetFocus();
										posDOC = (POSITION)1;
										break;
									}
								}

								if(posDOC == NULL)
								{
									CDocument* pDocument = (CDocument*)pTemplate->OpenDocumentFile(NULL);
									if(pDocument == NULL) break;

									CString sDocTitle;
									sDocTitle.Format(_T("%s"), sCaption);
									pDocument->SetTitle(sDocTitle);
								}
							}
							else
							{
								CDocument* pDocument = (CDocument*)pTemplate->OpenDocumentFile(NULL);
								if(pDocument == NULL) break;
								
								CString sDocTitle;
								sDocTitle.Format(_T("%s"), sCaption);
								pDocument->SetTitle(sDocTitle);
							}

							break;
						}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
					}	// while(pos != NULL)
				} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
				*ret = 1;
			}	// if (nTableIndex == idx)
		}	// for (UINT i = 0;i < vecIndex.size();i++)
	}	// if (vecIndex.size() > 0)
	else
	{
		*ret = 0;
	}
}

// Use this function, when calling from inside another Suite/User module; 060619 p�d
void DLL_BUILD OpenSuiteEx(_user_msg *msg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sFuncStr;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sLangSet;
	CString sFileToOpen;
	int nTableIndex;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(pApp != NULL);

	sModuleFN = getModuleFN(hInst);

	if (vecIndex.size() == 0)
		return;

	sLangSet = getLangSet();

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		nTableIndex = vecIndex[i].nTableIndex;
		if (nTableIndex == msg->getIndex())
		{
			sVecIndexTableModuleFN = vecIndex[i].szSuite;
			sLangFN = vecIndex[i].szLanguageFN;

			sFileToOpen = msg->getFileName();

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		sResStr.LoadString(nTableIndex);

		if (g_pXML->Load(sLangFN))
		{
			sCaption = g_pXML->str(nTableIndex);
		}

		if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			POSITION pos = pApp->GetFirstDocTemplatePosition();
			while(pos != NULL)
			{
				pTemplate = pApp->GetNextDocTemplate(pos);
				pTemplate->GetDocString(sDocName, CDocTemplate::docName);
				ASSERT(pTemplate != NULL);
				sDocName = '\n' + sDocName;

				if (pTemplate && sDocName.Compare(sResStr) == 0)
				{
					if(bIsOneInst == TRUE)
					{
						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						while(posDOC != NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
							POSITION pos = pDocument->GetFirstViewPosition();
							if(pos != NULL)
							{
								CView* pView = pDocument->GetNextView(pos);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								break;
							}
						}

						if(posDOC == NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->OpenDocumentFile(NULL);
							if(pDocument == NULL) break;

							CString sDocTitle;
							sDocTitle.Format(_T("%s"), sCaption);
							pDocument->SetTitle(sDocTitle);
						}
					}
					else
					{
						CDocument* pDocument = (CDocument*)pTemplate->OpenDocumentFile(NULL);
						if(pDocument == NULL) break;

						CString sDocTitle;
						sDocTitle.Format(_T("%s"), sCaption);
						pDocument->SetTitle(sDocTitle);
					}

					break;
				}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
			}	// while(pos != NULL)
		} // if (sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}

}


/* ------------------------------------------------------------------
	Get license-information for a module
------------------------------------------------------------------ */
int DLL_BUILD GetLicense(TCHAR *szName, LICENSE *licRet, TCHAR *szSuite, BOOL bShow)
{
	CString csBuf;
	csBuf.Format(_T("%s"), szName);

	int nIndex = theApp.GetLicense(csBuf);
	if(nIndex == -1)
	{
		return FALSE;
	}

	// check if this license is a demo, if it timed out etc.
	LICENSE lic = theApp.m_caLicenses.GetAt(nIndex);
	BOOL bLicense = TRUE;
	if(lic.bDemo == TRUE)	// demo?
	{
		if(lic.nRemDays == 0)	// no remaining demo days
		{
			bLicense = FALSE;
			csBuf.Format(_T("%s\n\n%s"), g_pXML->str(20), g_pXML->str(50));	// "Utv�rderingstid slut."
			int nRet = MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + szSuite, MB_YESNO|MB_ICONEXCLAMATION);
			if(nRet == IDYES)	// show license window
			{
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
					(LPARAM)&_user_msg(820, _T("OpenSuiteEx"),	
					_T("License.dll"),
					_T(""),
					_T(""),
					_T("")));
			}
		}
		else
		{
			if(bShow == TRUE)
			{
				if(lic.nRemDays == -1)
					csBuf.Format(_T("%s"), g_pXML->str(19));	// "Demo."
				else
					csBuf.Format(_T("%s %d %s"), g_pXML->str(19), lic.nRemDays, g_pXML->str(21));	// "Demo. xx dag(ar) kvar."

				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + szSuite, MB_OK|MB_ICONINFORMATION);
			}
		}
	}
	else
	{
		if(lic.nType == 6)	// concurrent user
		{
			if(lic.nRemLic == -1)
			{
				bLicense = FALSE;	// no licenses left
				csBuf.Format(_T("%s"), g_pXML->str(47));
				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + szSuite, MB_OK|MB_ICONEXCLAMATION);
			}
		}
	}

	*licRet = lic;	//= theApp.m_caLicenses.GetAt(nIndex);

	return (int)bLicense;
}

/* ------------------------------------------------------------------
	Check if a module is licensed (windows message version)
------------------------------------------------------------------ */
void DLL_BUILD CheckLicense(_user_msg *pMsg, CWnd *wnd, vecINDEX_TABLE &vecIndex, int *ret)
{
	theApp.CheckLicense(pMsg->getName(), pMsg);
	*ret = 1;
}

/* ------------------------------------------------------------------
	Decrease a license-counter by 1 (windows message version)
------------------------------------------------------------------ */
void DLL_BUILD DecrCounter(_user_msg *pMsg,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	theApp.DecrCounter(pMsg->getName(), pMsg);
	*ret = 1;
}



/* ------------------------------------------------------------------
	get a module from memory or the license-file
------------------------------------------------------------------ */
int CSampleSuite::GetLicense(CString csLicense)
{
	BOOL bFound = FALSE;
	LICENSE lic;
	int nIndex = -1;

	// check if this license is already checked.
	// if so, it is not necessary to check in the license-file
	for(int nLoop=0; nLoop<m_caLicenses.GetSize(); nLoop++)
	{
		lic = m_caLicenses.GetAt(nLoop);

		if(lic.csModID == csLicense)	// we found it!
		{
			bFound = TRUE;
			nIndex = nLoop;
			break;
		}
	}

	// we did not find the requested license module in the license file
	if(bFound == FALSE)
	{
		// show a messagebox stating the problem
		AfxMessageBox(g_pXML->str(57), MB_ICONERROR);
	}

	return nIndex;
}

int CSampleSuite::CheckLicense(CString csLicense, _user_msg* pMsg)
{
	WriteToLog("Checklicense");

	// check if we have a license file
	if(m_bLicfile == FALSE)
	{
		// try to open it
		OpenLicense();
		GatherLicenses();
	}

	if(m_bLicfile == FALSE || m_csLicfilePath == _T(""))
	{
		AfxMessageBox(g_pXML->str(45));

		CLicenseDlg dlg;
		dlg.m_csPath = m_csLicfilePath;
		int nRet = dlg.DoModal();

		if(nRet == IDOK)
		{
			m_csLicfilePath = dlg.m_csPath;

			if(m_csLicfilePath.GetAt(m_csLicfilePath.GetLength()-1) != '\\')
				m_csLicfilePath += _T("\\");

			regSetStr(REG_ROOT, _T("HMS_License\\Settings"), _T("LicfilePath"), m_csLicfilePath);

			// reload the license file
			OpenLicense();
			GatherLicenses();
		}
		else
		{
			WriteToLog("Checklicense - no valid path to license file");
			return -1;
		}
	}

	if(m_bLicfile == FALSE || m_csLicfilePath == _T(""))
	{
		WriteToLog("Checklicense - missing license file or path to license file");
		_user_msg msg(pMsg->getIndex(), _T("0"), _T("0"), _T("0"), _T("0"), _T("0"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}

	// check if we still can read the license file
	long lRefresh = proxyLicProtector.Refresh();
	if(lRefresh != 0)
	{
		WriteToLog("Checklicense - could not update license file");
		AfxMessageBox(g_pXML->str(55), MB_ICONERROR);	// "could not update license file!"
		_user_msg msg(pMsg->getIndex(), _T("0"), _T("0"), _T("0"), _T("0"), _T("0"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}

	int nRet = GatherLicense(csLicense);
	if(nRet == -1)
	{
		WriteToLog("Checklicense - error in GatherLicense()");
		_user_msg msg(pMsg->getIndex(), _T("0"), _T("0"), _T("0"), _T("0"), _T("0"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}

	LICENSE lic;
	int nIndex = GetLicense(pMsg->getName());
	if(nIndex == -1)
	{
		WriteToLog("Checklicense - error in GetLicense()");
		_user_msg msg(pMsg->getIndex(), _T("0"), _T("0"), _T("0"), _T("0"), _T("0"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}
	lic = m_caLicenses.GetAt(nIndex);

	// kolla att taggen st�mmer mot avs�ndarmodulen
	CString csBuf = (TCHAR*)pMsg->getVoidBuf();
	if( lic.csTag.CompareNoCase(csBuf) != 0)
	{
		csBuf.Format(_T("Checklicense - tag mismatch (%s - %s)"), lic.csTag, csBuf);
		WriteToLog(CStringA(csBuf));
		_user_msg msg(pMsg->getIndex(), _T("0"), _T("0"), _T("0"), _T("0"), _T("0"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}

	if(lic.nType == 1)	// user
	{
		if(lic.bDemo == TRUE)
			nRet = lic.nRemLic;
	}
	else if(lic.nType == 2)	// computer
	{
		if(lic.bDemo == TRUE)
			nRet = lic.nRemLic;
	}
	else if(lic.nType == 6)	// concurrent user
	{
		if(lic.bDemo == TRUE)
			nRet = lic.nRemLic;
	}
	else if(lic.nType == 4)	// YesNo
	{
		if(lic.bYesNo == FALSE)
			nRet = -1;
		else
			nRet = 1;
	}
	else if(lic.nType == 3)	// counter
	{
		nRet = lic.nRemLic;
	}
	else if(lic.nType == 5)	// ITEM counter
	{
		nRet = lic.nRemLic;
	}


	// check if this license is a demo, if it timed out etc.
	BOOL bLicense = TRUE;
	if(lic.bDemo == TRUE)	// demo?
	{
		if(lic.nRemDays == 0)	// no remaining demo days
		{
			bLicense = FALSE;
			csBuf.Format(_T("%s\n\n%s"), g_pXML->str(20), g_pXML->str(50));	// "Utv�rderingstid slut."
			int nRet = MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + pMsg->getFileName(), MB_YESNO|MB_ICONEXCLAMATION);
			if(nRet == IDYES)	// show license window
			{
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
					(LPARAM)&_user_msg(820, _T("OpenSuiteEx"),	
					_T("License.dll"),
					_T(""),
					_T(""),
					_T("")));
			}
		}
		else
		{
			if(_tcscmp(pMsg->getArgStr(), _T("1")) == 0)	// show messagebox
			{
				if(lic.nRemDays == -1)
					csBuf.Format(_T("%s"), g_pXML->str(19));	// "Demo."
				else
					csBuf.Format(_T("%s %d %s"), g_pXML->str(19), lic.nRemDays, g_pXML->str(21));	// "Demo. xx dag(ar) kvar."

				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + pMsg->getFileName(), MB_OK|MB_ICONINFORMATION);
			}
		}
	}
	else
	{
		if(lic.nType == 6)	// concurrent user
		{
			if(lic.nRemLic == -1)
			{
				bLicense = FALSE;	// no licenses left
				csBuf.Format(_T("%s"), g_pXML->str(47));
				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csBuf, g_pXML->str(820) + _T(": ") + pMsg->getFileName(), MB_OK|MB_ICONEXCLAMATION);
			}
		}
	}

	// amount of days/users/etc..
	CString csType, csLic, csRemDays, csRemLic, csDemo;
	csBuf.Format(_T("%d"), bLicense);
	csDemo.Format(_T("%d"), lic.bDemo);

	_user_msg msg(pMsg->getIndex(), csBuf, csBuf, csBuf, csBuf, csDemo);
	memcpy(pMsg, &msg, sizeof(_user_msg));

	WriteToLog("Checklicense OK");
	return 0;
}

int CSampleSuite::DecrCounter(CString csLicense, _user_msg* pMsg)
{
	CString csBuf;
	LICENSE lic;
	int nIndex = GetLicense(csLicense);
	if(nIndex == -1)
	{
		_user_msg msg(pMsg->getIndex(), _T("-1"), _T("-1"), _T("-1"), _T("-1"), _T("-1"));
		memcpy(pMsg, &msg, sizeof(_user_msg));
		return -1;
	}
	lic = m_caLicenses.GetAt(nIndex);

	// decrease the module by 1
	if(lic.nRemLic > 0)
	{
		csBuf.Format(_T("%d"), lic.nRemLic);
		long lRes = proxyLicProtector.SetVal(pMsg->getName(), _T("TLI"), csBuf);

		if(lRes == 0)	// no errors
		{
			lic.nRemLic--;
			m_caLicenses.SetAt(nIndex, lic);
		}
		else
		{
			AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes), MB_ICONERROR);
		}
	}

	// return the remaining count
	CString csType, csLic;
	csType.Format(_T("%d"), lic.nType);
	csLic.Format(_T("%d"), lic.nRemLic);

	_user_msg msg(pMsg->getIndex(), _T(""), _T(""), _T(""), csType, csLic);
	memcpy(pMsg, &msg, sizeof(_user_msg));

	return 0;
}

// add new modules to the local license file
int CSampleSuite::UpdateLicensefile()
{
	CArray<LICENSE, LICENSE> caLicenses;
	LICENSE licOld, lic;
	CString csUpdateLic;
	csUpdateLic = getProgDir() + _T("Setup\\HaglofManagementSystems.lic");

	// we have a "update-file" ?
	CFile cfLic;
	if( cfLic.Open(csUpdateLic, CFile::modeRead) == 0)
	{
		return FALSE;
	}
	cfLic.Close();

	// close the main license file
	long lRes = proxyLicProtector.Quit();
	if(lRes != 0)
	{
		AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes), MB_OK|MB_ICONERROR);
	}


	WriteToLog("Update licensefile");
	// Call prepare to open the licence file
	long nRes = proxyLicProtector.Prepare(csUpdateLic, sPSK + sPSK2);
	if(nRes != 0)
	{
		AfxMessageBox(proxyLicProtector.GetErrorMessage(nRes), MB_OK|MB_ICONERROR);
		return FALSE;
	}
	proxyLicProtector.RemoveLocalRunNo();


	// make sure that no one has screwed with the clock or the license file
	if( !CheckTampered() ) return FALSE;

	// loop through all modules
	int nCurPos = 0, nLoop;
	CString csBuf, csRes, csType, csBuf2;
	long lValidate, lRemDays, lRemLic, lTotLic;

	csBuf = proxyLicProtector.GetModuleList(false, true);
	csRes = csBuf.Tokenize(_T(";"), nCurPos);
	if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);

	while (csRes != _T("") && csType != _T(""))
	{
		lic.csModID = csRes;

		// check if this module exist
		// if not, add it to the array
		BOOL bFound = FALSE;
		for(nLoop=0; nLoop<m_caLicenses.GetSize(); nLoop++)
		{
			licOld = m_caLicenses.GetAt(nLoop);

			if(licOld.csModID == lic.csModID)
			{
				bFound = TRUE;
				break;
			}
		}

		if(bFound == FALSE)	// new one
		{
			lic.bAllowDeactivate = FALSE;
			lic.nWebActivation = 0;
			lic.nNoOfDays = 0;
			lic.nRemDays = 0;
			lic.nRemLic = 0;

			if(csType == _T("U"))	// user
				lic.nType = 1;
			else if(csType == _T("S"))	// computer
				lic.nType = 2;
			else if(csType == _T("R"))	// concurrent user
				lic.nType = 6;
			else if(csType == _T("Y"))	// YesNo
				lic.nType = 4;
			else if(csType == _T("C"))	// counter
				lic.nType = 3;
			else if(csType == _T("I"))	// item counter
				lic.nType = 5;

			if(lic.nType == 4)	// YesNo
			{
				lValidate = proxyLicProtector.ValidatesYes(csRes);

				if(lValidate == 0)
				{
					lic.bYesNo = TRUE;
					lic.nLicenses = 1;	// yes
					lic.bDemo = FALSE;
				}
				else if(lValidate == 1)
				{
					lic.bYesNo = TRUE;
					lic.nLicenses = 1;	// yes
					lic.bDemo = TRUE;
				}
				else if(lValidate == 3)
				{
					lic.bYesNo = FALSE;
					lic.nLicenses = 0;	// no
					lic.bDemo = FALSE;
				}
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
			}
			else if(lic.nType == 6)	// concurrent user
			{
				lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);
				lRemLic = proxyLicProtector.RemainingLicences(csRes);

				if(lValidate == 1 || lValidate == 16 || lValidate == 17)	// demo version
					lic.bDemo = TRUE;
				else if(lValidate == 0)	// no demo
					lic.bDemo = FALSE;
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
				else
				{
					lic.bDemo = TRUE;

					// concurrent users
					if(lValidate == 32)	// no free licenses
					{
						long lLicenses = proxyLicProtector.TotalLicences(csRes);
						if(lLicenses == 1)	// we have only 1 license, and its in use
						{
							// try if it is a dead client
							csBuf2 = proxyLicProtector.GetEntriesOfModule(csRes, true, _T(";"));
							proxyLicProtector.DeactivateEntry(csRes, csBuf2);
							proxyLicProtector.RemoveDeactivated(csRes);

							// test again
							lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);
							if(lValidate == 0)	// it was a dead user
							{
								lic.bDemo = FALSE;
								lRemLic = 0;
							}
							else if(lValidate == 1)	// demo
							{
								lic.bDemo = TRUE;
								lRemLic = 0;
							}
							else if(lValidate == 128)	// can not write to license file
							{
								m_caLicenses.RemoveAll();
								theApp.m_bLicRefreshed = TRUE;

								MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

								return 1;				
							}
							else	// license still in use
							{
								lic.bDemo = FALSE;
								lRemLic = -1;
							}
						}
						else
						{
							lic.bDemo = FALSE;
							lRemLic = -1;
						}
					}
				}
			}
			else
			{
				lValidate = proxyLicProtector.Validate(csRes, _T(""), false);

				if(lValidate == 1)	// demo version
					lic.bDemo = TRUE;
				else if(lValidate == 0)	// no demo
					lic.bDemo = FALSE;
				else if(lValidate == 128)	// can not write to license file
				{
					m_caLicenses.RemoveAll();
					theApp.m_bLicRefreshed = TRUE;

					MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

					return 1;				
				}
				else
					lic.bDemo = TRUE;
			}

			lic.nNoOfDays = proxyLicProtector.NoOfDays(csRes);

			lic.nLicenses = proxyLicProtector.TotalLicences(csRes);

			lic.dtExpires = proxyLicProtector.ExpiredOn(csRes);
			lic.dtMaxDate = lic.dtExpires; //proxyLicProtector.GetVal(csRes, "MEO");

			lRemDays = proxyLicProtector.RemainingDays(csRes);
			lic.nRemDays = (int)lRemDays;

			if(lic.nType != 6)
			{
				lRemLic = proxyLicProtector.RemainingLicences(csRes);
			}
			lic.nRemLic = (int)lRemLic;

			lic.csModName = proxyLicProtector.ModuleName(lic.csModID);

			lic.csTag = proxyLicProtector.GetTagValue(lic.csModID);

			// add this new license to the update-array
			caLicenses.Add(lic);
		}


		// get next module
		csRes = csBuf.Tokenize(_T(";"), nCurPos);
		if(csRes != _T("")) csType = csBuf.Tokenize(_T(";"), nCurPos);
	};

	// close the license file
	lRes = proxyLicProtector.Quit();
	if(lRes != 0)
	{
		AfxMessageBox(proxyLicProtector.GetErrorMessage(lRes), MB_OK|MB_ICONERROR);
	}


	// open local license file and update if needed
	CString csLicFile;
	csLicFile.Format(_T("%sHaglofManagementSystems.lic"), m_csLicfilePath);

	nRes = proxyLicProtector.Prepare(csLicFile, sPSK + sPSK2);
	if(nRes != 0)
	{
		AfxMessageBox(proxyLicProtector.GetErrorMessage(nRes), MB_OK|MB_ICONERROR);
		m_bLicfile = FALSE;
		return FALSE;
	}

	// add any new modules to the local license file
	for(nLoop=0; nLoop<caLicenses.GetSize(); nLoop++)
	{
		lic = caLicenses.GetAt(nLoop);

		nRes = proxyLicProtector.AddModule(lic.csModID, lic.csModName, (long)lic.nType,
			(long)lic.nLicenses, lic.bDemo, lic.dtExpires, (long)lic.nNoOfDays,
			lic.dtMaxDate, lic.csTag, lic.bAllowDeactivate, (long)lic.nWebActivation);

		// add to the local array aswell
		m_caLicenses.Add(lic);
	}
	WriteToLog("Update licensefile OK");

	return TRUE;
}

int CSampleSuite::GetCopyProtection()
{
	return (int)proxyLicProtector.GetCopyProtection();
}

int CSampleSuite::GatherLicense(CString csModule)
{
	// License stuff
	CString csBuf, csBuf2;
	LICENSE lic;
	CString sMsg;

	if(m_csLicfilePath == _T("")) return -1;

	// check if we have a license file
	if(m_bLicfile == FALSE)
	{
		// try to open it
		OpenLicense();
	}
	if(m_bLicfile == FALSE) return -1;


	// check if the Shell is activated
	CString csLic;

	m_bTampered = FALSE;
	if( proxyLicProtector.get_LicenceTampered() )	// licensefile has been messed with
	{
		m_bTampered = TRUE;
	}

	m_bDateChanged = FALSE;
	if( proxyLicProtector.get_SysdateChanged() )	// date has been changed atleast 1 day backwards
	{
		m_bDateChanged = TRUE;
	}


	long lRes = proxyLicProtector.GetCopyProtection();
	if(lRes == 0)
		m_bActivated = FALSE;
	else
		m_bActivated = TRUE;
	
	// are we on a USB-memory?
	if( proxyLicProtector.GetInstCode(11) != _T("") )
	{
		// make sure that the license file is in the root directory
		int nFound = m_csLicfilePath.Find('\\');
		if( m_csLicfilePath.Find('\\', nFound+1) != -1 )
		{
			::MessageBox(0, g_pXML->str(52), g_pXML->str(820), MB_OK|MB_ICONEXCLAMATION); //_T("License file must be on the root of the USB-memory!"));
			m_bTampered = TRUE;
			return -1;
		}
	}

	if(m_bTampered == FALSE && m_bDateChanged == FALSE)
	{
		// get the modules
		CString csRes, csType, csBuf2;
		int nCurPos = 0;
		long lValidate, lRemDays, lRemLic;
		BOOL bFirst = TRUE;


		csRes = csModule;
		csType = proxyLicProtector.GetVal(csRes, _T("LTY"));

		if(csType == _T("U"))	// user
			lic.nType = 1;
		else if(csType == _T("S"))	// computer
			lic.nType = 2;
		else if(csType == _T("R"))	// concurrent user
			lic.nType = 6;
		else if(csType == _T("Y"))	// YesNo
			lic.nType = 4;
		else if(csType == _T("C"))	// counter
			lic.nType = 3;
		else if(csType == _T("I"))	// item counter
			lic.nType = 5;
		else
		{
			// we assume that the module does not even exist,
			// so show a messagebox stating the problem
			AfxMessageBox(g_pXML->str(57), MB_ICONERROR);
			return -1;
		}

		proxyLicProtector.SetCheckInterval(csRes, 15);	// set a longer delay between updates

		if(lic.nType == 4)	// YesNo
		{
			lValidate = proxyLicProtector.ValidatesYes(csRes);

			if(lValidate == 0)
			{
				lic.bYesNo = TRUE;
				lic.bDemo = FALSE;
			}
			else if(lValidate == 1)
			{
				lic.bYesNo = TRUE;
				lic.bDemo = TRUE;
			}
			else if(lValidate == 3)
			{
				lic.bYesNo = FALSE;
				lic.bDemo = FALSE;
			}
			else if(lValidate == 128)	// can not write to license file
			{
				m_caLicenses.RemoveAll();
				theApp.m_bLicRefreshed = TRUE;

				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

				return 1;				
			}
		}
		else if(lic.nType == 6)	// concurrent user
		{
			lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);
			lRemLic = proxyLicProtector.RemainingLicences(csRes);

			if(lValidate == 1 || lValidate == 16 || lValidate == 17)	// demo version
			{
				lic.bDemo = TRUE;
			}
			else if(lValidate == 0)	// no demo
			{
				lic.bDemo = FALSE;
			}
			else if(lValidate == 128)	// can not write to license file
			{
				m_caLicenses.RemoveAll();
				theApp.m_bLicRefreshed = TRUE;

				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

				return 1;				
			}
			else
			{
				// concurrent users
				if(lValidate == 32)	// no free licenses
				{
					CStringA csBuf;
					csBuf.Format("GatherLicense(): No free concurrent licenses (%s+%s)", m_csComputername, m_csUsername);
					WriteToLog(csBuf);

					long lLicenses = proxyLicProtector.TotalLicences(csRes);
					if(lLicenses == 1)	// we have only 1 license, and its in use
					{
						// try if it is a dead client
						proxyLicProtector.RemoveAllItems(csRes);

						WriteToLog("GatherLicense(): Sleep and retest license");
						long lInterval = proxyLicProtector.CheckInterval(csRes);
						Sleep(lInterval * 2000);

						// test again
						lValidate = proxyLicProtector.Validate(csRes, m_csComputername + _T("+") + m_csUsername, false);

						if(lValidate == 0)	// it was a dead user
						{
							lic.bDemo = FALSE;
							lRemLic = 0;
						}
						else if(lValidate == 1)	// demo
						{
							lic.bDemo = TRUE;
							lRemLic = 0;
						}
						else if(lValidate == 128)	// can not write to license file
						{
							m_caLicenses.RemoveAll();
							theApp.m_bLicRefreshed = TRUE;

							MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);
							WriteToLog("GatherLicense() Can not write to license file");

							return 1;				
						}
						else	// license still in use
						{
							WriteToLog("GatherLicense(): License still in use");
							lic.bDemo = FALSE;
							lRemLic = -1;
						}
					}
					else
					{
						lic.bDemo = FALSE;
						lRemLic = -1;
					}
				}
			}
		}
		else
		{
			lValidate = proxyLicProtector.Validate(csRes, _T(""), false);

			if(lValidate == 1)	// demo version
				lic.bDemo = TRUE;
			else if(lValidate == 0)	// no demo
				lic.bDemo = FALSE;
			else if(lValidate == 128)	// can not write to license file
			{
				m_caLicenses.RemoveAll();
				theApp.m_bLicRefreshed = TRUE;

				MessageBox(AfxGetMainWnd()->GetSafeHwnd(), g_pXML->str(54), g_pXML->str(820), MB_OK|MB_ICONERROR);

				return 1;				
			}
			else
				lic.bDemo = TRUE;
		}

		lic.nNoOfDays = proxyLicProtector.NoOfDays(csRes);
		lic.nWebActivation = proxyLicProtector.GetWebActivation(csRes);
		lic.dtExpires = proxyLicProtector.ExpiredOn(csRes);

		lRemDays = proxyLicProtector.RemainingDays(csRes);
		lic.nRemDays = (int)lRemDays;

		if(lic.nType != 6) lRemLic = proxyLicProtector.RemainingLicences(csRes);
		lic.nRemLic = (int)lRemLic;

		lic.csModID = csRes;

		lic.csModName = proxyLicProtector.ModuleName(lic.csModID);

		lic.csTag = proxyLicProtector.GetTagValue(lic.csModID);

		LICENSE licOld;
		for(int nLoop=0; nLoop<m_caLicenses.GetSize(); nLoop++)
		{
			licOld = m_caLicenses.GetAt(nLoop);
			if(licOld.csModID == lic.csModID)
			{
				m_caLicenses.SetAt(nLoop, lic);
				break;
			}
		}
	}

	theApp.m_bLicRefreshed = TRUE;

	return 1;
}

// THREADS

DWORD WINAPI CheckRefresh(LPVOID lpParam)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	int nMissing = 0;

	while(1)
	{
		//if(proxyLicProtector.Refresh() == 2) // 2 = Licence file was not found or not opened; this function is not thread-safe and may thus corrupt the licence file
		if(((CSampleSuite*)lpParam)->m_bLicfile == TRUE)	// license file was not found
		{
			if( (hFind = FindFirstFile(((CSampleSuite*)lpParam)->m_csLicfilePath + _T("HaglofManagementSystems.lic"), &fd)) == INVALID_HANDLE_VALUE )
			{
				nMissing++;
			}
			else	// we could find license file, reset the quit counter
			{
				FindClose(hFind); hFind = NULL;
				nMissing = 0;
			}

			if( nMissing == 1 )	// show this message only once
			{
				CString csBuf;
				csBuf.Format(_T("%s\r\n%s"), g_pXML->str(45), g_pXML->str(56));
				AfxGetApp()->GetMainWnd()->MessageBox(csBuf, g_pXML->str(820), MB_OK|MB_ICONWARNING);	// "Can not read license file"
			}
			else if( nMissing == 120 ) // kill the application after X seconds
			{
				AfxGetApp()->GetMainWnd()->MessageBox(g_pXML->str(53), g_pXML->str(820), MB_OK|MB_ICONERROR);	// "Can not read license file, program will quit!"
				AfxGetApp()->PostThreadMessage(WM_QUIT, 0, 0);
				return -1;
			}
		}

		Sleep(1000);
	}

	return 0;
}

int CSampleSuite::CheckTampered()
{
	BOOL bTampered = FALSE;
/*	if( proxyLicProtector.get_LicenceTampered() )	// licensefile has been messed with
	{
		bTampered = TRUE;
		AfxMessageBox(g_pXML->str(41), MB_OK|MB_ICONERROR);
	}*/

	BOOL bDateChanged = FALSE;
	if( proxyLicProtector.get_SysdateChanged() )	// date has been changed atleast 1 day backwards
	{
		bDateChanged = TRUE;
		AfxMessageBox(g_pXML->str(42), MB_OK|MB_ICONERROR);
	}
	if(bTampered || bDateChanged)
	{
		CString csLicFile;
		csLicFile.Format(_T("%sHaglofManagementSystems.lic"), m_csLicfilePath);

		long nRes = proxyLicProtector.Prepare(csLicFile, sPSK + sPSK2);
		if(nRes != 0)
		{
			AfxMessageBox(proxyLicProtector.GetErrorMessage(nRes), MB_OK);
			m_bLicfile = FALSE;
			return FALSE;
		}
		
		return FALSE;
	}

	return TRUE;
}

void CSampleSuite::WriteToLog(CStringA message, bool error/*=false*/)
{
	char szTime[256];
	SYSTEMTIME st;

	if( !message.IsEmpty() )
	{
		// Set up timestamp prefix
		GetLocalTime(&st);
		_snprintf(szTime, sizeof(szTime), "%d-%02d-%02d %02d:%02d:%02d:\t", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	}
	else
	{
		// Insert empty row with no timestamp if message is empty
		szTime[0] = NULL;
	}

	cout << szTime;
	cout << "License.dll: ";
	if( error )
	{
		cout << "[ERROR] ";
	}
	cout << message << "\n";
}

void CSampleSuite::GatherHwids()
{
	// get the hardware-id
	CString csCode;

	WriteToLog("Gather hwids");

	m_caInstCodes.RemoveAll();
	for(int nLoop=1; nLoop<13; nLoop++)
	{
		csCode = proxyLicProtector.GetInstCode(nLoop);

		if(csCode != _T("B3F5AA23"))
			m_caInstCodes.Add(csCode);
		else
			m_caInstCodes.Add(_T(""));	// l�gg till tom kod
	}

	WriteToLog("Gather hwids OK");
}
