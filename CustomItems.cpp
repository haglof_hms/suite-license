// Custom Items.cpp : implementation file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "CustomItems.h"

////////////////////////////////////////////////////////////////////////////////////////////////

CCustomItemButton::CCustomItemButton(CString strCaption, CString strValue)
	: CXTPPropertyGridItem(strCaption, strValue)
{
	m_csValue = strValue;
	m_nFlags = xtpGridItemHasExpandButton|xtpGridItemHasEdit;
}

CCustomItemButton::~CCustomItemButton(void)
{
}

void CCustomItemButton::OnInplaceButtonDown()
{
}

void CCustomItemButton::SetValue(CString strValue)
{
	m_csValue = strValue;

	CXTPPropertyGridItem::SetValue(m_csValue);
}







CCustomCheckButton::CCustomCheckButton(BOOL bChecked)
	: CXTPReportRecordItem()
{
	m_bHasCheckbox = TRUE;

	SetEditable(bChecked);
}

CCustomCheckButton::~CCustomCheckButton(void)
{
}
