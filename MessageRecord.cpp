// MessageRecord.cpp: implementation of the CMessageRecord class.
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// �1998-2005 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MessageRecord.h"
#include "CustomItems.h"
#include "samplesuite.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


IMPLEMENT_SERIAL(CMessageRecordItemCheck, CXTPReportRecordItem, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)
IMPLEMENT_SERIAL(CMessageRecordItemCombo, CXTPReportRecordItem, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)
IMPLEMENT_SERIAL(CMessageRecordItemFilebox, CXTPReportRecordItem, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCheck

CMessageRecordItemCheck::CMessageRecordItemCheck(BOOL bCheck)
{
	HasCheckbox(TRUE);
	SetChecked(bCheck);
}

int CMessageRecordItemCheck::GetGroupCaptionID(CXTPReportColumn* /*pColumn*/)
{
	return FALSE;
}

int CMessageRecordItemCheck::Compare(CXTPReportColumn* /*pColumn*/, CXTPReportRecordItem* pItem)
{
	return int(IsChecked()) - int(pItem->IsChecked());
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CMessageRecord, CXTPReportRecord, VERSIONABLE_SCHEMA | _XTP_SCHEMA_CURRENT)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessageRecord::CMessageRecord()
{
	CreateItems();
}

CMessageRecord::CMessageRecord(
		CString csName,
		CString csVersion,
		CString csArticle,
		CString csKeys,
		CString csStatus,
		BOOL bReg
		)
{
	CXTPReportRecordItem *pItem;

	pItem = AddItem(new CXTPReportRecordItemText(csName));
	pItem->SetEditable(FALSE);
	pItem = AddItem(new CXTPReportRecordItemText(csVersion));
	pItem->SetEditable(FALSE);
	pItem = AddItem(new CXTPReportRecordItemText(csArticle));
	pItem->SetEditable(FALSE);
	pItem = AddItem(new CXTPReportRecordItemText(csKeys));
	pItem->SetEditable(FALSE);
	pItem = AddItem(new CXTPReportRecordItemText(csStatus));
	pItem->SetEditable(FALSE);

	if(bReg == FALSE)
		AddItem(new CXTPReportRecordItemText(_T("")));
	else
		AddItem(new CCustomCheckButton(bReg));
}

void CMessageRecord::CreateItems()
{
	// Initialize record items with empty values
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CXTPReportRecordItemText(_T("")));
	AddItem(new CCustomCheckButton(FALSE));
}

CMessageRecord::~CMessageRecord()
{
}

BOOL CMessageRecord::SetRead()
{
	return TRUE;
}

void CMessageRecord::GetItemMetrics(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pItemMetrics)
{
	if( pDrawArgs->pItem->GetCaption(0).Find(g_pXML->str(20)) >= 0 )	// evaluation expired
	{
		pItemMetrics->clrBackground = RGB(0xff, 0, 0);
	}
	else if( pDrawArgs->pItem->GetCaption(0).Find(g_pXML->str(19)) >= 0 )	// demo
	{
		pItemMetrics->clrBackground = RGB(0xff, 0xff, 0);
	}
	else if( pDrawArgs->pItem->GetCaption(0).Find(g_pXML->str(23)) >= 0 )	// registrated, active
	{
		pItemMetrics->clrBackground = RGB(0, 0xff, 0);
	}
}

void CMessageRecord::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecord::DoPropExchange(pPX);
}

//////////////////////////////////////////////////////////////////////
// CMessageRecord class

IMPLEMENT_SERIAL(CKeyRecord, CXTPReportRecord, 0)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CKeyRecord::CKeyRecord()
{
	CreateItems();
}

CKeyRecord::CKeyRecord(CString csFilename, int nType)
{
	CXTPReportRecordItem* pItem = AddItem(new /*CXTPReportRecordItemText*/CMessageRecordItemFilebox(csFilename));
	pItem->SetItemData(0);

	pItem = AddItem(new CMessageRecordItemCombo(nType));
	pItem->SetItemData(1);
}

void CKeyRecord::CreateItems()
{
	// Initialize record items with empty values
}

CKeyRecord::~CKeyRecord()
{
}

//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemCombo

CMessageRecordItemCombo::CMessageRecordItemCombo(int nLevel)
{
	m_nValue = nLevel;

	GetEditOptions(NULL)->AddConstraint(_T(""), 0);
	GetEditOptions(NULL)->AddConstraint(_T("DP"), 1);
	GetEditOptions(NULL)->AddConstraint(_T("DPDOC"), 2);
	GetEditOptions(NULL)->m_bConstraintEdit = TRUE;
	GetEditOptions(NULL)->AddComboButton();
}

CString CMessageRecordItemCombo::GetCaption(CXTPReportColumn* /*pColumn*/)
{
	CXTPReportRecordItemConstraint* pConstraint = GetEditOptions(NULL)->FindConstraint(m_nValue);
	ASSERT(pConstraint);
	return pConstraint->m_strConstraint;
}

void CMessageRecordItemCombo::OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS*, CXTPReportRecordItemConstraint* pConstraint)
{
	m_nValue = (int)pConstraint->m_dwData;
}


//////////////////////////////////////////////////////////////////////////
// CMessageRecordItemFilebox

CMessageRecordItemFilebox::CMessageRecordItemFilebox(LPCTSTR szText)
	: CXTPReportRecordItem(), m_strText(szText)
{
	GetEditOptions(NULL)->AddExpandButton();
}

void CMessageRecordItemFilebox::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	SetValue(szText);
}

CString CMessageRecordItemFilebox::GetCaption(CXTPReportColumn* /*pColumn*/)
{
	if (!m_strCaption.IsEmpty())
		return m_strCaption;

	if (m_strFormatString == _T("%s"))
		return m_strText;

	CString strCaption;
	strCaption.Format(m_strFormatString, (LPCTSTR)m_strText);
	return strCaption;
}


void CMessageRecordItemFilebox::DoPropExchange(CXTPPropExchange* pPX)
{
	CXTPReportRecordItem::DoPropExchange(pPX);
	PX_String(pPX, _T("Text"), m_strText, _T(""));
}

void CMessageRecordItemFilebox::OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
{
	CXTPReportRecordItem::OnInplaceButtonDown(pButton);
}
