//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SampleSuite.rc
//
#define IDD_LICENSE                     820
#define IDR_LICENSE                     820
#define IDR_SETTINGS                    821
#define IDR_TOOLBAR_PROGRAMS            7002
#define IDC_REPORTCTRL                  7007
#define IDC_TABCONTROL                  7008
#define IDC_PLACEHOLDER                 7040
#define IDD_CONTACT                     7040
#define IDD_KEYS                        7041
#define IDD_INVOICE                     7042
#define IDC_EDIT1                       7076
#define IDC_EDIT_NAME                   7076
#define IDC_EDIT_PATH                   7076
#define IDC_EDIT2                       7077
#define IDC_EDIT_ADDRESS1               7077
#define IDC_EDIT3                       7078
#define IDC_EDIT_ADDRESS2               7078
#define IDC_EDIT4                       7079
#define IDC_EDIT_COUNTRY                7079
#define IDC_EDIT_NAME2                  7080
#define IDC_EDIT6                       7081
#define IDC_EDIT_EMAIL                  7081
#define IDC_EDIT7                       7082
#define IDC_EDIT_COMPANY                7082
#define IDC_EDIT8                       7083
#define IDC_EDIT_TELEPHONE              7083
#define IDC_EDIT_COMPANY2               7084
#define IDC_EDIT_ADDRESS3               7085
#define IDC_EDIT_ADDRESS4               7086
#define IDC_EDIT_ACTKEY                 7087
#define IDC_EDIT_ADDRESS5               7087
#define IDC_EDIT_INSTCODE               7088
#define IDC_EDIT_COUNTRY2               7088
#define IDC_BUTTON1                     7089
#define IDC_EDIT_TELEPHONE2             7089
#define IDC_EDIT_EMAIL2                 7090
#define IDC_EDIT_ADDRESS6               7091
#define IDC_LIST1                       7093
#define IDC_STATIC_ACTKEY               7094
#define IDC_LIST_LICENSES               7095
#define IDC_EDIT_CUSTNUM                7097
#define IDC_EDIT_VAT                    7098
#define IDC_STATIC_CUSTNUM              7099
#define IDC_STATIC_NAME                 7100
#define IDC_STATIC_COMPANY              7101
#define IDC_STATIC_VAT                  7102
#define IDC_STATIC_ADDRESS              7103
#define IDC_STATIC_COUNTRY              7104
#define IDC_STATIC_TELEPHONE            7105
#define IDC_STATIC_EMAIL                7106
#define IDC_STATIC_LICENSES             7107
#define IDC_STATIC_CODES                7108
#define IDC_LIST_CODES                  7109
#define IDC_CHECK_INVOICE               7110
#define IDC_EDIT_CUSTNUM2               7111
#define IDC_STATIC_CUSTNUM2             7112
#define IDC_STATIC_COMPANY2             7113
#define IDC_STATIC_NAME2                7114
#define IDC_EDIT_VAT2                   7115
#define IDC_STATIC_ADDRESS2             7116
#define IDC_STATIC_EMAIL2               7117
#define IDC_STATIC_TELEPHONE2           7118
#define IDC_STATIC_COUNTRY2             7119
#define IDC_STATIC_VAT2                 7120
#define IDC_EDIT_NOTES                  7123
#define IDC_STATIC_NOTES                7124
#define IDC_STATIC_INFO                 7126
#define IDC_STATIC_PATH                 7127
#define IDC_BUTTON_CHOOSE_PATH          7128
#define IDC_CUSTOM1                     7129
#define IDC_BUTTON_COPYCLIP             7130
#define IDC_REPORT_LICENSES             32770
#define ID_BUTTON_INFO                  32771
#define ID_BUTTON_ORDER                 32772
#define ID_BUTTON_KEYS                  32784
#define ID_BUTTON_PATH                  32785
#define ID_BUTTON_UP                    32797
#define ID_BUTTON_DOWN                  32798
#define ID_BUTTON_SEND                  32799
#define ID_BUTTON32800                  32800
#define ID_BUTTON_ACTIVATE              32800

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7045
#define _APS_NEXT_COMMAND_VALUE         32803
#define _APS_NEXT_CONTROL_VALUE         7131
#define _APS_NEXT_SYMED_VALUE           7008
#endif
#endif
